<%@include file="/Menu/Header.jsp" %>
<body onload="load_years()">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <%@include file="/Menu/Petrofill_Top_Menu.jsp" %>

            <div class="container-fluid main-container">
                <div class="col-md-2 sidebar">
                    <div class="row">
                        <!-- uncomment code for absolute positioning tweek see top comment in css -->
                        <div class="absolute-wrapper"> </div>
                        <!-- Menu -->
                        <div class="side-menu">

                            <%@include file="/Menu/Admin_Menu.jsp" %>
                        </div>
                    </div>
                </div>
                <div class="col-md-10 content">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <h1>Shift Allocation</h1><hr/>
                                         <div id="midcontent" style="margin-left:150px ; height:auto;" >
                    
                   
                        
                   
                   
                         <%if (session.getAttribute("Transaction") != null) { %>
                                <script type="text/javascript"> 
                    
                    var m = noty({text: '<%=session.getAttribute("Transaction")%>',
                        layout: 'center',
                        type: 'Success',
 
                         timeout: 4800});
                    
                </script> <%
                session.removeAttribute("Transaction");
                            }

                        %>
                        <!--creating random user id-->
                         <%!
    public int generateRandomNumber(int start, int end ){
        Random random = new Random();
        long fraction = (long) ((end - start + 1 ) * random.nextDouble());
        return ((int)(fraction + start));
    }
%>  
                        
                        
                        
                        
                   
                   
                    <p><font color="red">*</font> indicates must fill fields</p>
                            <form action="procurement_warehouse" method="post" class="form-horizontal">
                        <br/>
                        <table cellpadding="2px" cellspacing="3px" border="0px" style="margin-left:10px ;">
                       
                            <tr>
                                <td><label for="first_name">Type<font color="red">*</font></label></td>
                                <td ><select name="type_code" id="type_code" class="textbox2 form-control" onchange="filter_name_item_description(this)" required>
                                         <option value="">Choose Type</option>
                                        
                                    </select>
                                </td>
                                <td><label for="first_name">Location<font color="red">*</font></label></td>
                                <td ><select name="class_code" id="class_code" class="textbox2 form-control" onchange="" required>
                                         <option value="">Choose Location</option>
                                        
                                    </select>
                                </td>
                            </tr>
                       
                            <tr>
                                <td></td>
                                 <td class="align_button_left"><label id="shift_label_open_date" for="first_name">Shift Open Date<font color="red">*</font></label></td>
                                <td><input id="shift_open_date" type=text required name="shift_open_date" placeholder="dd/mm/yyyy" class="form-control"/></td>
                                <td></td>
                               
                            </tr>
                            </tbody>
                        </table>
                       <table id="meter_generated" cellpadding="1px" cellspacing="1px" border="0px" style="margin-left:10px ;">
                            <thead id="meters" ><tr><th></th><th>Electronic Mtr</th><th>Manual Mtr</th><th>Variance</th></tr></thead>
                            <tbody>
                            <tr>
                                <td class="align_button_left"><label id="opening_meter_no" for="first_name">Opening Meter No:<font color="red">*</font></label></td>
                                <td><input id="Opening_Meter_No1" type=text required placeholder="Please insert figures" name="Opening_Meter_No1" value="" student_name class="form-control"/></td>
                                <td><input id="Opening_Meter_No2" type=text placeholder="Please insert figures" required name="Closing_Meter_No2" student_name class="form-control"/></td>
                                <td><input id="Opening_Variance" type=text placeholder="Please insert figures" required name="Opening_Variance" student_name class="form-control"/></td>
                                </tr> 
                            
                            <thead id="lubricants"><tr><th></th><th></th><th>No of Opening Lubes</th></tr></thead>
                            <tr>
                                <td></td>
                                <td class="align_button_left"><label id="opening_label_lubes" for="first_name">Lubes Figures:<font color="red">*</font></label></td>
                                <td><input id="Opening_No_Lubes" type=text placeholder="Please insert figures"  name="Opening_No_Lubes" student_name class="textbox3 form-control"/></td>
                                
                            </tr>
                            
                            <tr>
                                <td></td>
                                <td><label id="Staff_Allocated_shift" for="first_name">Staff Allocated shift<font color="red">*</font></label></td>
                                <td ><select id="staff_code" name="staff_code" id="staff_code" class="textbox2 form-control" onchange="load_staff()" required>
                                         <option value="">Choose Staff</option>
                                        
                                    </select>
                                </td>
                            </tr>


                           <tr> 
                               <td></td>
                               <td class="align_button_left"><input  size="12px" class="btn btn-info" type="reset" value="Clear" /></td> 
                               <td></td>
                               <td class="align_button_left"><input type="submit"  class="btn btn-info" value="Allocate Shift" onmouseover="return compare();"/></td>
                            </tr>
                        </table>
                    </form>
                        </div>                        </div>

                    </div>
                </div>
            </div>

            <%@include file="/Menu/Petrofill_Footer.jsp" %>

        </div>
    </div>
</div>
<!-- jQuery -->
<script>
    $.noConflict();
</script>
<!--<script src="js/jquery-1.11.3.min.js"></script>-->
 <!--Bootstrap JavaScript--> 
</body>

</html>