<%@include file="/Menu/Header.jsp" %>
	<body>
   <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <%@include file="/Menu/Petrofill_Top_Menu.jsp" %>
          
          <div class="container-fluid main-container">
      <div class="col-md-2 sidebar">
         <div class="row">
   <!-- uncomment code for absolute positioning tweek see top comment in css -->
   <div class="absolute-wrapper"> </div>
   <!-- Menu -->
   <div class="side-menu">
     
       <%@include file="/Menu/Admin_Menu.jsp" %>
   </div>
</div>
</div>
      <div class="col-md-10 content">
                         <div class="row">
      <div class="col-sm-12">
         <!--
            ****** NAVIGATION WIDGET *******
            -->
<!--         <div id="nav-widget" class="btn-group btn-group-justified">
            <div class="btn-group">
               <button type="button" class="btn text-uppercase"><i class="fa fa-map-marker fa-2x"></i>  Check In<span class="badge">6</span></button>
            </div>
            <div class="btn-group">
               <button type="button" class="btn text-uppercase"><i class="fa fa-bell fa-2x"></i>  Events <span class="badge">6</span></button>
            </div>
            <div class="btn-group">
               <button type="button" class="btn text-uppercase"><i class="fa fa-user fa-2x"></i>  Account</button>
            </div>
            <div class="btn-group">
               <button type="button" class="btn text-uppercase"><i class="fa fa-cog fa-2x"></i>  Settings</button>
            </div>
         </div>-->
      </div>
   </div>
   
   
   <div class="row">
      <!-- COLUMN ONE -->
      <div class="col-sm-6 col-md-4">
         <!--
            ****** LINE CHART WIDGET *******
            -->    
         <div id="line-chart-widget" class="panel">
            <div class="panel-heading">
               <h4 class="text-uppercase"><strong>Apple Inc.</strong><span class="label pull-right">107.26 <i class="fa fa-plus"></i>0.23(0.10%)</span><br><small>Nasdaq: AAPL</small></h4>
            </div>
            <div class="panel-body">
               <canvas id="myLineChart"></canvas>
            </div>
            <div class="panel-footer">
               <div class="list-block">
                  <ul class="text-center legend">
                     <li>
                        <h3>13.5 M</h3>
                        Shares Traded
                     </li>
                     <li>
                        <h3>28.44 B</h3>
                        Market Cap
                     </li>
                  </ul>
               </div>
               <div class="chart-block clearfix">
                  <div class="pull-left">
                     Monthly Volume
                     <canvas id="myBarChart"></canvas>
                  </div>
                  <div class="pull-right">
                     Yearly Change<br>
                     <div class="change text-center"><i class="fa fa-plus"></i> 86.01</div>
                  </div>
               </div>
            </div>
         </div>
       
      </div>
      
      
      <!-- COLUMN TWO -->   
      <div class="col-sm-6 col-md-4">
         <!--
            ****** CHART WIDGET *******
            -->    
         <div id="pie-chart-widget" class="panel">
            <div class="panel-heading text-center">
               <h5 class="text-uppercase"><strong>Data Transfer</strong></h5>
            </div>
            <div class="panel-body">
               <canvas id="myPieChart"></canvas>
            </div>
            <div class="panel-footer">
               <div class="list-block">
                  <ul class="text-center legend">
                     <li class="video" style="margin-right: 1px;">
                        video 
                        <h2>62%</h2>
                     </li>
                     <li class="photo">
                        photo 
                        <h2>21%</h2>
                     </li>
                     <li class="audio" style="margin-left: 1px;">
                        audio 
                        <h2>10%</h2>
                     </li>
                  </ul>
               </div>
               <div class="btn-group btn-group-justified text-uppercase text-center">
                  <a class="btn btn-default" role="button"><i class="fa fa-cloud-upload fa-2x"></i><br><small>Upload Files</small></a>
                  <a class="btn btn-default" role="button"><i class="fa fa-share-alt fa-2x"></i><br><small>Share Link</small></a>
                  <a class="btn btn-default" role="button"><i class="fa fa-history fa-2x"></i><br><small>Back Up</small></a>
               </div>
            </div>
         </div>
      
      </div>
      
      
      <!-- COLUMN THREE -->
      <div class="col-sm-6 col-md-4 ">
         <!--
            ****** PROFILE WIDGET *******
            -->
         <div id="profile-widget" class="panel">
            <div class="panel-heading">
            </div>
            <div class="panel-body">
               <div class="media">
                  <a class="pull-left" href="#">
                  <img class="media-object img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/flashmurphy/128.jpg">
                  </a>
                  <div class="media-body">
                     <h2 class="media-heading">John Raymons</h2>
                     Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  </div>
               </div>
            </div>
            <div class="panel-footer">
               <div class="btn-group btn-group-justified">
                  <a class="btn btn-default" role="button"><i class="fa fa-eye"></i> 172</a>
                  <a class="btn btn-default" role="button"><i class="fa fa-comment"></i> 34</a>
                  <a class="btn btn-default highlight" role="button"><i class="fa fa-heart"></i> 210</a>
               </div>
            </div>
         </div>

      </div>
   </div>
</div>
      </div>
        
<%@include file="/Menu/Petrofill_Footer.jsp" %>

   </div>
      </div>
   </div>
	  <!-- jQuery -->
      <script src="js/jquery-1.11.3.min.js"></script>
      <!-- Bootstrap JavaScript -->
      <script src="js/bootstrap.js"></script>
   </body>
    
</html>