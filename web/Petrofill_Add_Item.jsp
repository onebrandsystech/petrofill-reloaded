<%-- 
    Document   : Petrofill_Add_Item
    Created on : Feb 15, 2015, 7:57:39 PM
    Author     : Hare Cheka Arnold
--%>

<%@page import="java.util.Calendar"%>
<%@page import="java.util.Random"%>
<%
    response.setHeader("Cache-Control", "no-store"); //HTTP 1.1
    response.setHeader("Pragma", "no-cache"); //HTTP 1.0
    response.setDateHeader("Expires", 0); //prevents caching at the proxy server

    if (session.getAttribute("userid")==null || session.getAttribute("userid")==null ) {
        response.sendRedirect("index.jsp");
    } 
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/main.css"/>
         
         <link rel="stylesheet" type="text/css" href="js/jquery-ui.css"/>
         <link rel="shortcut icon" href="images/PetroFillIcon1.png"/>
        <title>PetroFill Add Item/Product</title>
      
	<script type="text/javascript" src="js/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <link href="css/style.css" media="screen" rel="stylesheet" type="text/css" />
		
		<script src="menu/prefix-free.js"></script>  
   <script type="text/javascript" src="js/jquery-1.9.1.js"></script>

<script type="text/javascript" src="js/noty/jquery.noty.js"></script>

<script type="text/javascript" src="js/noty/layouts/top.js"></script>
<script type="text/javascript" src="js/noty/layouts/center.js"></script>
<!-- You can add more layouts if you want -->

<script type="text/javascript" src="js/noty/themes/default.js"></script>
      <script type="text/javascript">
           
         
           
           
            function checkPasswords() {
                var password = document.getElementById('password');
                var conf_password = document.getElementById('conf_password');

                if (password.value != conf_password.value) {
                    conf_password.setCustomValidity('Passwords do not match');
                } else {
                    conf_password.setCustomValidity('');
                }
            } 
$(function() {
$( "#datepicker" ).datepicker();
});

//Getting Suppliers
function load_suppliers(){    

var xmlhttp;    

if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("supplier").innerHTML=xmlhttp.responseText;

document.getElementById("week").innerHTML="<option>loading..</option>";
}
}
xmlhttp.open("POST","Petrofill_Supplier_Loader",true);
xmlhttp.send();
}
//Getting the Categories
 function load_categories(){    
//Receiving variables
load_suppliers();
   
var xmlhttp;    

if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{

document.getElementById("category").innerHTML=xmlhttp.responseText;

document.getElementById("week").innerHTML="<option>loading..</option>";
}
}
xmlhttp.open("POST","Petrofill_Item_Category_loader",true);
xmlhttp.send();
}  




function filter_districts(district){

var dist=district.value;    

// window.open("districtchooser?county="+dist.value);     
var xmlhttp;    
if (dist=="")
{
//filter the districts    



document.getElementById("district").innerHTML="<option value=\"\">Choose District</option>";
return;
}
if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("district").innerHTML=xmlhttp.responseText;
}
}
xmlhttp.open("POST","district_loader?county_id="+dist,true);
xmlhttp.send();
}//end of filter districts     
function load_units(district){

var dist=district.value;        
var xmlhttp;    
if (dist=="")
{
document.getElementById("sp").innerHTML="<option value=\"\">Choose Supply Unit</option>";
return;
}
if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("sp").innerHTML=xmlhttp.responseText;
}
}
xmlhttp.open("POST","sp_loader?dist_id="+dist,true);
xmlhttp.send();
}     
        </script> 
        <SCRIPT language=Javascript>
		<!--
function isCharacterKey(evt){

var charCode=(evt.which) ? evt.which : event.keyCode
if(charCode > 31 && (charCode < 48 || charCode>57))
return true;
return false;
}

function isNumberKey(evt){

var charCode=(evt.which) ? evt.which : event.keyCode
if(charCode > 31 && (charCode < 48 || charCode>57))
return false;
return true;
}

//-->
</SCRIPT>
        
    </head>
    <body onload="load_categories();">
       
     <div id="container" style="height:auto;" >
 <%if(session.getAttribute("level")!=null){ if(session.getAttribute("level").equals("1")){%>    
<%@include file="/menu/adminmenu.html" %>
<%}else if(session.getAttribute("level").equals("2")){%>
<%@include file="/menu/user1menu.jsp" %>
<%}else if(session.getAttribute("level").equals("5")){%>
<%@include file="/menu/user2menu.jsp" %>
<%}} else{}%>

              <div id="header" align="center">   
              </div>
<!--            <br>-->
            
            <div id="content" style="height:auto; width: 900px;">
                <%if (session.getAttribute("fullname")!=null){ %>
                <div style="margin-left: 20px; margin-top: 10px">
                 Hi, <u><%=session.getAttribute("fullname").toString()%></u>   
                    
                </div><br> <%}%>
                 <div style="margin-left: 100px; margin-top: -10px; color: #000; background: greenyellow; font-size: 20px; text-align: center;">Add New Item. </div>
             
                <div id="midcontent" style="margin-left:150px ; height:auto;" >
                    
                   
                        
                   
                   
                         <%if (session.getAttribute("user_added") != null) { %>
                                <script type="text/javascript"> 
                    
                    var n = noty({text: '<%=session.getAttribute("user_added")%>',
                        layout: 'center',
                        type: 'Success',
 
                         timeout: 5000});
                    
                </script> <%
                session.removeAttribute("user_added");
                            }

                        %>
                        <!--creating random user id-->
                         <%!
    public int generateRandomNumber(int start, int end ){
        Random random = new Random();
        long fraction = (long) ((end - start + 1 ) * random.nextDouble());
        return ((int)(fraction + start));
    }
%>  
                        
                        
                        
                        
                   
                   
                    <p><font color="red">*</font> indicates must fill fields</p>
                    <form action="Petrofill_Add_Item" method="post">
                        <br/>
                        <table cellpadding="2px" cellspacing="1px" border="0px" style="margin-left:5px ;">
                                         
                            <tr>
                                <td class="align_button_left"><label for="first_name">Item Name<font color="red">*</font></label></td>
                                <td><input id="item_name" type=text required name="item_name"  student_name class="textbox"/></td>
                                <td class="align_button_left"><label for="first_name">Item Code<font color="red">*</font></label></td>
                                <td><input id="item_code" type=text required name="item_code" readonly="true" student_name class="textbox"/></td>
                            </tr>
                            <tr>
                                <td class="align_button_left"><label for="first_name">Measuring Units<font color="red">*</font></label></td>
                                <td><input id="measuring_units" type=text required name="measuring_units"  student_name class="textbox"/></td>
                                                  
                               <td class="align_button_left"><label for="">Supplier <font color="red">*</font></label></td>
                                <td ><select name="supplier" id="supplier" class="textbox2" required>
                                         
                                        
                                    </select>
                                </td>
                                </tr>
                                <tr>
                               <td class="align_button_left"><label for="town">Description <font color="red"></font></label></td>
                            <td><textarea id="item_description" name="item_description" class="textbox" value="" placeholder="Enter Item Description" rows="8" column="30" ></textarea><br/></td>
                            
                           <td class="align_button_left"><label for="">Category <font color="red">*</font></label></td>
                                <td ><select name="category" id="category" class="textbox2" required>
                                         <option value="">Choose Category</option>
                                        
                                        
                                    </select>
                                </td>
                                </tr>
                           <tr>
                               <td class="align_button_left"><label for="first_name">Cost Price<font color="red">*</font></label></td>
                                <td><input id="cost_price" type=text required name="cost_price"  student_name class="textbox"/></td>
                           
                           <td class="align_button_left"><label for="first_name">Retail Price<font color="red">*</font></label></td>
                                <td><input id="retail_price" type=text required name="retail_price"  student_name class="textbox"/></td>
                           </tr>
                           <tr>
                                <td class="align_button_left"><label for="">Status <font color="red">*</font></label></td>
                                <td ><select name="status" id="status" class="textbox2" required>
                                         <option value="">Choose Status</option>
                                        <option value="1">Active</option>
                                        <option value="0">Suspended</option>
                                        
                                    </select>
                                </td>
                                    <td class="align_button_left"><label for="first_name">Reorder Level<font color="red">*</font></label></td>
                                <td><input id="reorder_level" type=text required name="reorder_level"  student_name class="textbox"/></td>
                                                     
                               </tr>
                           <tr>
                                <td class="align_button_left"><label for="first_name">VAT (16%)<font color="red">*</font></label></td>
                                <td ><select name="vat" id="vat" class="textbox2" required>
                                         <option value="">Choose</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                        
                                    </select>
                                </td>
                                <td class="align_button_left"><label for="first_name">Current Date<font color="red">*</font></label></td>
                                <td><input id="current_date" type=text required name="current_date" readonly="true" student_name class="textbox"/></td>
                                </tr>
            
                                      <%
Calendar cal = Calendar.getInstance();
int year= cal.get(Calendar.YEAR);              

%>
                           
                           <tr> 
                               <td class="align_button_left">
                                   <input  size="12px"  type="reset" value="clear" /></td> 
                               <td></td>
                               <td></td>
                               <td class="align_button_left"> <input type="submit" class="submit" value="Add" style="background-color: lightseagreen;" onmouseover="getAge();"/></td>
                            </tr>
                        </table>
                    </form>
                </div>
          
            </div>
<div id="footer">
    <p align="center" style=" font-size: 18px;"> &copy; CODEX SYSTEMS <%=year%></p>
            </div>
        </div>    
        
    </body>
</html>



