<%@include file="/Menu/Header.jsp" %>
    <body onload="load_years()">

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <%@include file="/Menu/Petrofill_Top_Menu.jsp" %>

            <div class="container-fluid main-container">
                <div class="col-md-2 sidebar">
                    <div class="row">
                        <!-- uncomment code for absolute positioning tweek see top comment in css -->
                        <div class="absolute-wrapper"> </div>
                        <!-- Menu -->
                        <div class="side-menu">

                            <%@include file="/Menu/Admin_Menu.jsp" %>
                        </div>
                    </div>
                </div>
                <div class="col-md-10 content">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <h1>Title</h1>
                            <hr/>
                            content
                        </div>
                    </div>
                </div>
            </div>

            <%@include file="/Menu/Petrofill_Footer.jsp" %>

        </div>
    </div>
</div>

</body>

</html>