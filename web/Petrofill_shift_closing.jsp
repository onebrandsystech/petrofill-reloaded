<%@include file="/Menu/Header.jsp" %>
    <body onload="load_years()">

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <%@include file="/Menu/Petrofill_Top_Menu.jsp" %>

            <div class="container-fluid main-container">
                <div class="col-md-2 sidebar">
                    <div class="row">
                        <!-- uncomment code for absolute positioning tweek see top comment in css -->
                        <div class="absolute-wrapper"> </div>
                        <!-- Menu -->
                        <div class="side-menu">

                            <%@include file="/Menu/Admin_Menu.jsp" %>
                        </div>
                    </div>
                </div>
                <div class="col-md-10 content">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <h1>Shift Closing</h1>
                            <hr/>
                            <div id="midcontent" style="margin-left:150px ; height:auto;" >





                                <%if (session.getAttribute("Transaction") != null) {%>
                                <script type="text/javascript">

                                    var m = noty({text: '<%=session.getAttribute("Transaction")%>',
                                        layout: 'center',
                                        type: 'Success',
                                        timeout: 4800});

                                </script> <%
                        session.removeAttribute("Transaction");
                    }

                                %>
                                <!--creating random user id-->
                                <%!
                                    public int generateRandomNumber(int start, int end) {
                                        Random random = new Random();
                                        long fraction = (long) ((end - start + 1) * random.nextDouble());
                                        return ((int) (fraction + start));
                                    }
                                %>  






                                <p><font color="red">*</font> indicates must fill fields</p>
                                <form action="procurement_warehouse" method="post">
                                    <br/>
                                    <table id="date_generated" cellpadding="2px" cellspacing="3px" border="0px" style="margin-left:10px ;">

                                        <tr>
                                            <td><label for="first_name">Type<font color="red">*</font></label></td>
                                            <td ><select name="type_code" id="type_code" class="textbox2 form-control" onchange="filter_name_item_description(this)" required>
                                                    <option value="">Choose Type</option>

                                                </select>
                                            </td>
                                            <td><label for="first_name">Location<font color="red">*</font></label></td>
                                            <td ><select name="class_code" id="class_code" class="textbox2 form-control" onchange="" required>
                                                    <option value="">Choose Location</option>

                                                </select>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="align_button_left"><label id="shift_open_date_label" for="first_name">Shift Open Date<font color="red">*</font></label></td>
                                            <td><input id="shift_open_date" type=text class="textbox form-control" value="" name="shift_open_date" student_name class="textbox"/></td>

                                            <td class="align_button_left"><label id="Shift_close_date_label" for="first_name">Shift Close Date<font color="red">*</font></label></td>
                                            <td><input type="text" onmousemove="" id="todaysdate" readonly="true" name="todaysdate" placeholder="dd-mm-yyyy" required class="textbox form-control"/></td>


                                        </tr>
                                    </table>
                                    <table id="lubricant_generated" cellpadding="2px" cellspacing="1px" border="0px" style="margin-left:10px ;"> 

                                        <thead id="lubricants"><tr><th></th><th>No of Opening Lubes</th><th>No of Closing Lubes</th></tr></thead>
                                        <tr>
                                            <td class="align_button_left"><label id="opening_label_lubes" for="first_name">Lubes Figures:<font color="red">*</font></label></td>
                                            <td><input id="Opening_No_Lubes" type=text placeholder="Please insert figures"  name="Opening_No_Lubes" student_name class="textbox form-control"/></td>
                                            <td><input id="Closing_No_Lubes" type=text placeholder="Please insert figures"  name="Closing_No_Lubes" student_name class="textbox form-control"/></td>
                                        </tr>
                                    </table>
                                    <table id="meter_generated" cellpadding="2px" cellspacing="1px" border="0px" style="margin-left:10px ;">
                                        <thead id="meters" ><tr><th></th><th>Electronic Mtr</th><th>Manual Mtr</th><th>Variance</th></tr></thead>
                                        <tbody>
                                            <tr>
                                                <td class="align_button_left"><label id="opening_meter_no" for="first_name">Opening Meter No:<font color="red">*</font></label></td>
                                                <td><input id="Opening_Meter_No1" type=text required placeholder="Please insert figures" name="Opening_Meter_No1" value="" student_name class="textbox form-control"/></td>
                                                <td><input id="Opening_Meter_No2" type=text placeholder="Please insert figures" required name="Closing_Meter_No2" student_name class="textbox form-control"/></td>
                                                <td><input id="Opening_Variance" type=text placeholder="Please insert figures" required name="Opening_Variance" student_name class="textbox"/></td>
                                            </tr>
                                            <tr>
                                                <td class="align_button_left"><label id="closing_meter_no"  for="first_name">Closing Meter No:<font color="red">*</font></label></td>
                                                <td><input id="Closing_Meter_No1" type=text required placeholder="Please insert figures" name="Closing_Meter_No1" value="" student_name class="textbox"/></td>
                                                <td><input id="Closing_Meter_No2" type=text placeholder="Please insert figures" required name="Closing_Meter_No2" student_name class="textbox"/></td>
                                                <td><input id="Closing_Variance" type=text placeholder="Please insert figures" required name="Closing_Variance" student_name class="textbox"/></td>
                                            </tr>
                                        </tbody>

                                    </table>
                                    <table id="Amount_generated" cellpadding="2px" cellspacing="1px" border="0px" style="margin-left:10px ;">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Expected Amount</th>
                                                <th>Delivered Amount</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>

                                                <td class="align_button_left"><label id="Cash_Amount_Label" for="first_name">Cash Amount(KSH):<font color="red">*</font></label></td>
                                                <td class="align_button_right"><input id="Expected_Amount" type=text  name="Expected_Amount" readonly="true" value ="" student_name class="textbox"/></td>
                                                <td class="align_button_right"><input id="Delivered_Amount" type=text  name="Delivered_Amount" readonly="true" value ="" student_name class="textbox"/></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table id="shift_employee" cellpadding="2px" cellspacing="1px" border="0px" style="margin-left:10px ;">
                                        <tr>
                                            <td class="align_button_left"><label id="employee_open_shift" for="first_name">Shift Opened By:<font color="red">*</font></label></td>
                                            <td><input id="open_by" type=text  name="open_by" readonly="true" value ="" student_name class="textbox"/></td>

                                            <td class="align_button_left"><label id="employee_close_shift" for="first_name">Shift Closed By:<font color="red">*</font></label></td>
                                            <td><input id="close_by" type=text style="background-color: #FBE3E4"   value=""  name="close_by" class="textbox"/></td>

                                        </tr>



                                        <tr> 
                                            <td></td>
                                            <td class="align_button_left"><input  size="12px"  type="reset" value="Clear" class="btn btn-info" /></td> 
                                            <td></td>
                                            <td class="align_button_left"><input type="submit" style="background-color: lightseagreen;" class="submit btn btn-info" value="Close Shift" onmouseover="return compare();"/></td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <%@include file="/Menu/Petrofill_Footer.jsp" %>
                  <script type="text/javascript">
           $(function() {

                $( document ).tooltip();
                $( "#accordion" ).accordion();
                
                $("#todaysdate").datepicker({
                    dateFormat: "dd/mm/yy",
                    changeMonth: true,
                    changeYear: true
                               
                });

            });
           
         
           
           
            function checkPasswords() {
                var password = document.getElementById('password');
                var conf_password = document.getElementById('conf_password');

                if (password.value != conf_password.value) {
                    conf_password.setCustomValidity('Passwords do not match');
                } else {
                    conf_password.setCustomValidity('');
                }
            } 
//********************************************************************************************************************************
function disabler() {
  //Receiving the determiner parameter   
  var v= document.getElementById("type_code").value;
  //Receiving variables
var shift_open_date_label = document.getElementById("shift_open_date_label");
var shift_open_date = document.getElementById("shift_open_date");
var Shift_close_date_label = document.getElementById("Shift_close_date_label");
var todaysdate = document.getElementById("todaysdate");
var lubricants = document.getElementById("lubricants");
var opening_label_lubes = document.getElementById("opening_label_lubes");
var Opening_No_Lubes = document.getElementById("Opening_No_Lubes");
var Closing_No_Lubes = document.getElementById("Closing_No_Lubes");
var meters = document.getElementById("meters");
var opening_meter_no = document.getElementById("opening_meter_no");
var Opening_Meter_No1 = document.getElementById("Opening_Meter_No1");
var Opening_Meter_No2 = document.getElementById("Opening_Meter_No2");
var Opening_Variance = document.getElementById("Opening_Variance");
var closing_meter_no = document.getElementById("closing_meter_no");
var Closing_Meter_No1 = document.getElementById("Closing_Meter_No1");
var Closing_Meter_No2 = document.getElementById("Closing_Meter_No2");
var Closing_Variance = document.getElementById("Closing_Variance");
//*******************tables***************************************
var Amount_generated = document.getElementById("Amount_generated");
var shift_employee = document.getElementById("shift_employee");
//****************************end of tables**********************
var Cash_Amount_Label = document.getElementById("Cash_Amount_Label");
var Expected_Amount = document.getElementById("Expected_Amount");
var Delivered_Amount = document.getElementById("Delivered_Amount");
var employee_open_shift = document.getElementById("employee_open_shift");
var open_by = document.getElementById("open_by");
var employee_close_shift = document.getElementById("employee_close_shift");
var close_by = document.getElementById("close_by");
 
//When choice is Pumps 
if(v==1) {
//Assigning features of the variables
shift_open_date_label.hidden = false;
shift_open_date.hidden = false;
Shift_close_date_label.hidden = false;
todaysdate.hidden = false;
lubricants.hidden = true;
opening_label_lubes.hidden = true;
Opening_No_Lubes.hidden = true;
Closing_No_Lubes.hidden = true;
meters.hidden = false;
opening_meter_no.hidden = false;
Opening_Meter_No1.hidden = false;
Opening_Meter_No2.hidden = false;
Opening_Variance.hidden = false;
closing_meter_no.hidden = false;
Closing_Meter_No1.hidden = false;
Closing_Meter_No2.hidden = false;
Closing_Variance.hidden = false;
Amount_generated.hidden = false;
shift_employee.hidden = false;
Cash_Amount_Label.hidden = false;
Expected_Amount.hidden = false;
Delivered_Amount.hidden = false;
employee_open_shift.hidden = false;
open_by.hidden = false;
employee_close_shift.hidden = false;
close_by.hidden = false;
 
 }
 //When Choice is Lubricants    
else if(v==2) {
  //Assigning features of the variables
shift_open_date_label.hidden = false;
shift_open_date.hidden = false;
Shift_close_date_label.hidden = false;
todaysdate.hidden = false;
lubricants.hidden = false;
opening_label_lubes.hidden = false;
Opening_No_Lubes.hidden = false;
Closing_No_Lubes.hidden = false;
meters.hidden = true;
opening_meter_no.hidden = true;
Opening_Meter_No1.hidden = true;
Opening_Meter_No2.hidden = true;
Opening_Variance.hidden = true;
closing_meter_no.hidden = true;
Closing_Meter_No1.hidden = true;
Closing_Meter_No2.hidden = true;
Closing_Variance.hidden = true;
Amount_generated.hidden = false;
shift_employee.hidden = false;
Cash_Amount_Label.hidden = false;
Expected_Amount.hidden = false;
Delivered_Amount.hidden = false;
employee_open_shift.hidden = false;
open_by.hidden = false;
employee_close_shift.hidden = false;
close_by.hidden = false;
  }
  //When Choice is neither Pump or Lubricant or Gas
  else{
  //Assigning features of the variables
shift_open_date_label.hidden = true;
shift_open_date.hidden = true;
Shift_close_date_label.hidden = true;
todaysdate.hidden = true;
lubricants.hidden = true;
opening_label_lubes.hidden = true;
Opening_No_Lubes.hidden = true;
Closing_No_Lubes.hidden = true;
meters.hidden = true;
opening_meter_no.hidden = true;
Opening_Meter_No1.hidden = true;
Opening_Meter_No2.hidden = true;
Opening_Variance.hidden = true;
closing_meter_no.hidden = true;
Closing_Meter_No1.hidden = true;
Closing_Meter_No2.hidden = true;
Closing_Variance.hidden = true;
Amount_generated.hidden = true;
shift_employee.hidden = true;
Cash_Amount_Label.hidden = true;
Expected_Amount.hidden = true;
Delivered_Amount.hidden = true;
employee_open_shift.hidden = true;
open_by.hidden = true;
employee_close_shift.hidden = true;
close_by.hidden = true;
  }}
//*******************************************************************************************************************************

       function load_counties(){    

// window.open("districtchooser?county="+dist.value);     
var xmlhttp;    

if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("county").innerHTML=xmlhttp.responseText;
}
}
xmlhttp.open("POST","county_loader",true);
xmlhttp.send();
}//county loader

//Gettinh the Categories
 function load_years(){    
//Receiving variables
var shift_open_date_label = document.getElementById("shift_open_date_label");
var shift_open_date = document.getElementById("shift_open_date");
var Shift_close_date_label = document.getElementById("Shift_close_date_label");
var todaysdate = document.getElementById("todaysdate");
var lubricants = document.getElementById("lubricants");
var opening_label_lubes = document.getElementById("opening_label_lubes");
var Opening_No_Lubes = document.getElementById("Opening_No_Lubes");
var Closing_No_Lubes = document.getElementById("Closing_No_Lubes");
var meters = document.getElementById("meters");
var opening_meter_no = document.getElementById("opening_meter_no");
var Opening_Meter_No1 = document.getElementById("Opening_Meter_No1");
var Opening_Meter_No2 = document.getElementById("Opening_Meter_No2");
var Opening_Variance = document.getElementById("Opening_Variance");
var closing_meter_no = document.getElementById("closing_meter_no");
var Closing_Meter_No1 = document.getElementById("Closing_Meter_No1");
var Closing_Meter_No2 = document.getElementById("Closing_Meter_No2");
var Closing_Variance = document.getElementById("Closing_Variance");
//*******************tables***************************************
var Amount_generated = document.getElementById("Amount_generated");
var shift_employee = document.getElementById("shift_employee");
//****************************end of tables**********************
var Cash_Amount_Label = document.getElementById("Cash_Amount_Label");
var Expected_Amount = document.getElementById("Expected_Amount");
var Delivered_Amount = document.getElementById("Delivered_Amount");
var employee_open_shift = document.getElementById("employee_open_shift");
var open_by = document.getElementById("open_by");
var employee_close_shift = document.getElementById("employee_close_shift");
var close_by = document.getElementById("close_by");
//Assigning features of the variables
shift_open_date_label.hidden = true;
shift_open_date.hidden = true;
Shift_close_date_label.hidden = true;
todaysdate.hidden = true;
lubricants.hidden = true;
opening_label_lubes.hidden = true;
Opening_No_Lubes.hidden = true;
Closing_No_Lubes.hidden = true;
meters.hidden = true;
opening_meter_no.hidden = true;
Opening_Meter_No1.hidden = true;
Opening_Meter_No2.hidden = true;
Opening_Variance.hidden = true;
closing_meter_no.hidden = true;
Closing_Meter_No1.hidden = true;
Closing_Meter_No2.hidden = true;
Closing_Variance.hidden = true;
Amount_generated.hidden = true;
shift_employee.hidden = true;
Cash_Amount_Label.hidden = true;
Expected_Amount.hidden = true;
Delivered_Amount.hidden = true;
employee_open_shift.hidden = true;
open_by.hidden = true;
employee_close_shift.hidden = true;
close_by.hidden = true;

// window.open("districtchooser?county="+dist.value);     
var xmlhttp;    

if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("type_code").innerHTML=xmlhttp.responseText;

document.getElementById("week").innerHTML="<option>loading..</option>";
}
}
xmlhttp.open("POST","Petrofill_Category_loadern",true);
xmlhttp.send();
}


function filter_name_description(){

// window.open("districtchooser?county="+dist.value);     
var xmlhttp;    

if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("cat_code").innerHTML=xmlhttp.responseText;
}
}
xmlhttp.open("POST","category_name_loader",true);
xmlhttp.send();
}
//end county loader

function filter_name_item_description(type_code){
disabler();
var petrofill_type=type_code.value; 
var xmlhttp;    
if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{

document.getElementById("class_code").innerHTML= xmlhttp.responseText;;

}
}
xmlhttp.open("POST","Petrofill_class_loader?petrofill_type="+petrofill_type,true);
xmlhttp.send();
}     
        </script> 
         
            <SCRIPT language=Javascript>
    function compare(){
                var inputdate = new Date($('#todaysdate').val());



                var dateOBj= new Date();

                var currdate=dateOBj.getDate();
                var curmonth=dateOBj.getMonth()+1;
                var curyear=dateOBj.getFullYear();
        
                var today=curyear+"/"+curmonth+"/"+currdate;    
                var todate = new Date(today); 

                //alert(todate+"  "+inputdate);

                if (inputdate < todate && inputdate!=null && inputdate!="")

                {

                    // Do something
                    alert("Date should not be in the past");
                    $('#todaysdate').focus();
                    return false;

                }
        
            }  
</SCRIPT>

        </div>
    </div>
</div>

</body>

</html>