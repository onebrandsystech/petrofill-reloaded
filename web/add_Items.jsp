<%-- 
    Document   : add_Items
    Created on : Aug 3, 2014, 7:54:25 PM
    Author     : Geofrey Nyabuto
--%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Random"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
	$(function() {
		$( "#dialog" ).dialog({
			autoOpen: false,
			show: {
				effect: "blind",
				duration: 500
			},
			hide: {
				effect: "explode",
				duration: 700
			}
		});

		$( "#opener" ).click(function() {
			$( "#dialog" ).dialog( "open" );
		});
	});
      </script> 
           function load_items(){  
var xmlhttp;    
if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("items").innerHTML=xmlhttp.responseText;
}
}
xmlhttp.open("POST","loadItems?",true);
xmlhttp.send();
            }
             function load_description(it){ 
                 var item=it.value;
var xmlhttp;    
if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("description").innerHTML=xmlhttp.responseText;
}
}
xmlhttp.open("POST","loadDescription?item="+item,true);
xmlhttp.send();
            }
            
             function load_source(){  
var xmlhttp;    
if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("source").innerHTML=xmlhttp.responseText;
}
}
xmlhttp.open("POST","loadSource?",true);
xmlhttp.send();
            }
            
          </script>
   function numbers(evt){
var charCode=(evt.which) ? evt.which : event.keyCode
if(charCode > 31 && (charCode < 48 || charCode>57))
return false;
return true;
}
//-->
</script>
<script>
    function insHeader()
{
var tbl = document.getElementById('tb');
var lastRow = tbl.rows.length;
var x=document.getElementById('tb').insertRow(lastRow)
var a=x.insertCell(0);
var m=x.insertCell(1);
var y=x.insertCell(2);
//var z=x.insertCell(3);
//var v=x.insertCell(4);
//var q=x.insertCell(5);
//var l=x.insertCell(6);
var hi = 1;
m.innerHTML="<font color='blue'><b>Field Name</b></font>";
y.innerHTML="<font color='blue'><b>value(s)</b></font>";
//z.innerHTML="<font color='blue'><b></b></font>";
//v.innerHTML="<font color='blue'><b>Item Details</b></font>";
a.innerHTML="<font color='blue'>No.</font>";
insRow();
}

function insRow()
{
   
var all_rows=document.getElementById("all_rows").value;

var all=parseInt(all_rows);
    var rws2=++all_rows;

   document.getElementById("all_rows").value=rws2;
var tbl = document.getElementById('tb');
var lastRow = tbl.rows.length;
var x=document.getElementById('tb').insertRow(lastRow)
var a=x.insertCell(0);
var m=x.insertCell(1);
var y=x.insertCell(2);
//var z=x.insertCell(3);
//var v=x.insertCell(4);
//var q=x.insertCell(5);
//var l=x.insertCell(6);
var hi = 1;
m.innerHTML="<input type=\"text\" id='fieldtype"+rws2+"' name='fieldtype"+rws2+"' class='textbox' style='width: 100px;border-color: green' >";
y.innerHTML="<textarea id='fieldvalue"+rws2+"' name='fieldvalue"+rws2+"' class='textbox' style='width: 300px;border-color: green'></textarea>";
//z.innerHTML="<textarea id='description"+rws2+"' required name='description"+rws2+"' value=''  style='width: 120px;border-color: green'></textarea>";
a.innerHTML=rws2;

}
function deleteRow()
{
    var all_rows=parseInt(document.getElementById("all_rows").value)+1;
   
//   alert(all_rows)
     if(parseInt(all_rows)<=1){
        
    }
    else{
      
    var rws2=--all_rows;
   
    if(rws2<0){
       rws2=0 ;
    }
document.getElementById('tb').deleteRow(all_rows);
 rws2=--all_rows;
//alert("all rows  :   "+rws2)
document.getElementById("all_rows").value=rws2;
}
}
    </script>
        $(document).ready( function(){
            $("#specialdata ").hide();
            $("#note_no ").hide();
            
          $("#special_features").change( function(){
          var data=$("#special_features") .val();
         if(data=="YES"){
              $("#specialdata ").show();
          
         }
         else{
         $("#specialdata ").hide();   
         }
          });  
           
         $("#delivery_notification").change( function(){
          var data=$("#delivery_notification") .val();
         if(data=="YES"){
              $("#note_no").val("");
              $("#note_no").show();
             $("#note_no").attr("required","true"); 
          
         }
         else{
          $("#note_no ").val("");
          $("#note_no ").removeAttr("required"); 
         $("#note_no ").hide();   
         }
          });
            
        })
        </script>
    </head>
    <body onload="insHeader();  ">
     <div id="container" style="height:auto;" >
         <div id="leftBar" style="margin-top: 8%;">
          <%@include file="menu.jsp"%>
          </div>
            
       <div style="margin-left:0px; width: 100.0%; margin-top: 0px; color: #000; background: #c4c4ff; font-size: 24px; text-align: center;">Receive items to the warehouse.<img src="images/help.png" id="opener" title="Click Here to view Help For this Page." alt=" Help Image " style=" width: 40px; height: 40px;"></h4></div>
                   
            <div id="content" style="height:auto; width: 900px;">
    <div id="dialog" title="Add Items Help." style=" font-size: 17px;">
<p>The fields marked (<font color="red">*</font>) are must enter fields.</p>
<p>The user id is system auto-generated.</p>

</div>
                <div id="midcontent" style="margin-left:40px ; height:auto;" >

                        
                        
<br>
                    <form action="addInventory" method="post">

                       
                        <br><input type="hidden" id="all_rows" name="all_rows" value="0" >
    <input type="hidden" id="total_rows" name="total_rows" value="">
    <i id="specialdata">
     <table style="width:90%;"> <tr>
               
               <td> <p style="margin-left: 20%;" onclick="insRow()">  Add Field:<img src="images/add1.png" title="Add Row"   style="width: 30px; height:30px;" alt="Add Row">
      </p>
            </td>
            <td>
      <p style="margin-left: 40%;" onclick="deleteRow()"> Delete field <img src="images/minus1.png" title="Delete Last Row"  style="width: 30px; height:30px;" alt="Delete Row">
      </p></td>
        </tr>
    </table>
    <br><br>
     <table cellpadding="2px" cellspacing="3px" border="1px" class="table_style" id="tb"  style="margin-left: 50px; background: #ccccff; width:800px;;">
</table>
    </i>
      <table cellpadding="2px" cellspacing="3px" border="0px" style="margin-left:450px ;">
              <tr> 
                               <td class="align_button_left"></td> <td class="align_button_right">
                               <input type="submit" class="submit" value="Add"/></td>
                            </tr>
                        </table>
                    </form>
                </div>
           </div>
        </div>    
        
    </body>
</html>
