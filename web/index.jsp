<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Petro Fill</title>

		<!-- Bootstrap CSS -->
		<link href="css/bootstrap.css" rel="stylesheet">
		<link href="css/font-awesome.css" rel="stylesheet">
                <link rel="shortcut icon" href="images/PetroFillIcon1.png"/>
<link href="css/style.css" media="screen" rel="stylesheet" type="text/css" />
        <link rel='stylesheet' type='text/css' href='css/main.css' />
                <script src="prefix-free.js"></script>
   
<script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>

<script type="text/javascript" src="js/noty/jquery.noty.js"></script>

<script type="text/javascript" src="js/noty/layouts/top.js"></script>
<script type="text/javascript" src="js/noty/layouts/center.js"></script>
<!-- You can add more layouts if you want -->

<script type="text/javascript" src="js/noty/themes/default.js"></script>

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
		<style type="text/css">
			body{
				background:url('images/petro-fuel.jpg') no-repeat;
				background-position: center top;
				background-size: cover;

			}
		</style>
	</head>
	<body>
		  <div class="container"> 

        <div id="loginbox" style="margin-top: 126px;" class="mainbox col-md-5 col-md-offset-3 col-sm-5 col-sm-offset-2">             

            <div id="login-widget" class="panel" >

            <form action="Petrofill_Login" method="post" id="loginform" class="form-horizontal" role="form">
               <div class="panel-heading text-center">
               <h2>Petro Fill</h2>
                  
               </div>
               <div class="panel-body">
                  <label for="login-email">Email</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                     <input id="login-email" type="text" class="form-control input-lg" name="uname" value="" placeholder="email">  
                  </div>
                  <label for="login-email">Password</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                     <input id="login-password" type="password" class="form-control input-lg" name="pass" placeholder="password">
                  </div>
               </div>
               <div class="panel-footer text-center">
                  <div class="login-btn">
                      <input type="submit" class="btn btn-ui btn-lg btn-block mrg-btm-10" value="Log In"/>
                   <!--  <a href="#" class="btn btn-ui btn-lg btn-block mrg-btm-10"><i class=""></i> Sign In</a>-->
                     <a href="#"><small>Forgot Username or Password?</small></a>
                  </div>
                  <a href="#" class="facebook"><i class=""></i> Register</a>             
               </div>
                <h4>
                        <%
                            if (session.getAttribute("error_login") != null)  { %>
                                <script type="text/javascript"> 
                    
                    var n = noty({text: '<%=session.getAttribute("error_login")%>',
                        layout: 'center',
                        type: 'Success',
 
                         timeout: 2800});
                    
                </script> <%
                
                session.removeAttribute("error_login");
                            }

                        %>
                        </h4>
            </form>
         </div> 
        </div>
        
    </div>
    <!-- jQuery -->
		<script src="js/jquery-1.11.3.min.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="js/bootstrap.js"></script>
   </body> 
</html>