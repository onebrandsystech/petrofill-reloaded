<%@include file="/Menu/Header.jsp" %>
<body onload="load_categories();">

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <%@include file="/Menu/Petrofill_Top_Menu.jsp" %>

            <div class="container-fluid main-container">
                <div class="col-md-2 sidebar">
                    <div class="row">
                        <!-- uncomment code for absolute positioning tweek see top comment in css -->
                        <div class="absolute-wrapper"> </div>
                        <!-- Menu -->
                        <div class="side-menu">

                            <%@include file="/Menu/Admin_Menu.jsp" %>
                        </div>
                    </div>
                </div>
                <div class="col-md-10 content">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <h1>Add Sub Category</h1>
                            <hr/>
                                         
                   <div id="midcontent" style="margin-left:150px ; height:auto;" >
                    
                   
                        
                   
                   
                         <%if (session.getAttribute("user_added") != null) { %>
                                <script type="text/javascript"> 
                    
                    var n = noty({text: '<%=session.getAttribute("user_added")%>',
                        layout: 'center',
                        type: 'Success',
 
                         timeout: 5000});
                    
                </script> <%
                session.removeAttribute("user_added");
                            }

                        %>
                        <!--creating random user id-->
                         <%!
    public int generateRandomNumber(int start, int end ){
        Random random = new Random();
        long fraction = (long) ((end - start + 1 ) * random.nextDouble());
        return ((int)(fraction + start));
    }
%>  
                        
                        
                        
                        
                   
                   
                    <p><font color="red">*</font> indicates must fill fields</p>
                    <form action="Petrofill_Add_Category" method="post">
                        <br/>
                        <table cellpadding="2px" cellspacing="3px" border="0px" style="margin-left:150px ;">
                                         
                            <tr>
                                <td class="align_button_left"><label for="first_name">Sub-Category Name<font color="red">*</font></label></td>
                                <td><input id="subcategory_name" type=text required name="subcategory_name"  student_name class="textbox"/></td></tr><tr>

                               
                            </tr>
                            <tr>
                                <td>
                                    
                                    
                                                           
                                </td>
                                 
                            </tr>
                          
                     

                            
                         
                           <tr> <td class="align_button_left"><label for="town">Description <font color="red"></font></label></td>
                            <td><textarea id="subcategory_description" name="subcategory_description" class="form-control" value="" placeholder="Enter SubCategory Description" rows="10" column="30" ></textarea><br/><br/></td></tr>
                            
                           <tr>
                           
                                <td class="align_button_left"><label for="">Choose Category <font color="red">*</font></label></td>
                                <td ><select name="category" id="category" class="textbox2 form-control" required>
                                         <option value="">Choose Category</option>
                                        
                                        
                                    </select>
                                </td>
                                <td></td>
                           </tr>
                           
                           <tr>
                           
                                <td class="align_button_left"><label for="">Choose Status <font color="red">*</font></label></td>
                                <td ><select name="status" id="status" class="textbox2 form-control" required>
                                         <option value="">Choose Status</option>
                                        <option value="1">Active</option>
                                        <option value="0">Suspended</option>
                                        
                                    </select>
                                </td>
                                <td></td>
                           </tr> 
            

                           
                           <tr> 
                               <td class="align_button_left"><input  size="12px"  type="reset" value="Clear" class="btn btn-info" /></td> <td class="align_button_right">
                               <input type="submit" class="submit btn btn-info" value="Add" style="background-color: lightseagreen;" onmouseover="getAge();"/></td>
                            </tr>
                        </table>
                    </form>
                </div>
                        </div>
                    </div>
                </div>
            </div>

            <%@include file="/Menu/Petrofill_Footer.jsp" %>

        </div>
    </div>
</div>

</body>

</html>