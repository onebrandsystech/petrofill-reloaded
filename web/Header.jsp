<%-- 
    Document   : Header
    Created on : Jun 22, 2015, 12:20:08 AM
    Author     : Hare Cheka Arnold
--%>
<%-- 
    Document   : Petrofill_shift_allocation
    Created on : Feb 3, 2015, 6:53:10 PM
    Author     : Hare Cheka Arnold
--%>

<%@page import="java.util.Calendar"%>
<%@page import="java.util.Random"%>
<%
    response.setHeader("Cache-Control", "no-store"); //HTTP 1.1
    response.setHeader("Pragma", "no-cache"); //HTTP 1.0
    response.setDateHeader("Expires", 0); //prevents caching at the proxy server

    if (session.getAttribute("userid") == null || session.getAttribute("userid") == null) {
        response.sendRedirect("index.jsp");
    }
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Petro Fill</title>

        <!-- Bootstrap CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/font-awesome.css" rel="stylesheet">

<!--        <link href="bio_js/css/start/jquery-ui-1.10.3.custom.css" rel="stylesheet"/>

        <link rel="stylesheet" href="bio_js/demos.css" />-->

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
                <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
                <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

<script type="text/javascript" src="js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<link href="css/style.css" media="screen" rel="stylesheet" type="text/css" />

<!--<script src="menu/prefix-free.js"></script>-->  
<script type="text/javascript" src="js/jquery-1.9.1.js"></script>

<script type="text/javascript" src="js/noty/jquery.noty.js"></script>

<script type="text/javascript" src="js/noty/layouts/top.js"></script>
<script type="text/javascript" src="js/noty/layouts/center.js"></script>
<!-- You can add more layouts if you want -->
<script type="text/javascript" src="js/noty/themes/default.js"></script>
<script src = "bio_js/jquery-ui-1.10.3.custom.js" ></script>
 <script type="text/javascript">
           $(function() {

                $( document ).tooltip();
                $( "#accordion" ).accordion();
                
                $("#shift_open_date").datepicker({
                    dateFormat: "dd/mm/yy",
                    changeMonth: true,
                    changeYear: true
                               
                });

            });
           
         
           
           
            function checkPasswords() {
                var password = document.getElementById('password');
                var conf_password = document.getElementById('conf_password');

                if (password.value != conf_password.value) {
                    conf_password.setCustomValidity('Passwords do not match');
                } else {
                    conf_password.setCustomValidity('');
                }
            } 


       function load_counties(){    

// window.open("districtchooser?county="+dist.value);     
var xmlhttp;    

if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("county").innerHTML=xmlhttp.responseText;
}
}
xmlhttp.open("POST","county_loader",true);
xmlhttp.send();
}//county loader




function filter_name_description(){

// window.open("districtchooser?county="+dist.value);     
var xmlhttp;    

if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("cat_code").innerHTML=xmlhttp.responseText;
}
}
xmlhttp.open("POST","category_name_loader",true);
xmlhttp.send();
}
//******************************************************************************************************************************
//Disables the component upon choice
function disabler() {
  //Receiving the determiner parameter   
  var v= document.getElementById("type_code").value;
  //Receiving variables
var type_code = document.getElementById("type_code");
var class_code = document.getElementById("class_code");
var shift_label_open_date = document.getElementById("shift_label_open_date");
var shift_open_date = document.getElementById("shift_open_date");
var meter_generated = document.getElementById("meter_generated");
var meters = document.getElementById("meters");
var opening_meter_no = document.getElementById("opening_meter_no");
var Opening_Meter_No1 = document.getElementById("Opening_Meter_No1");
var Opening_Meter_No2 = document.getElementById("Opening_Meter_No2");
var Opening_Variance = document.getElementById("Opening_Variance");
var lubricants = document.getElementById("lubricants");
//*******************tables***************************************
var opening_label_lubes = document.getElementById("opening_label_lubes");
var Opening_No_Lubes = document.getElementById("Opening_No_Lubes");
//****************************end of tables**********************
var Staff_Allocated_shift = document.getElementById("Staff_Allocated_shift");
var staff_code = document.getElementById("staff_code");
 
//When choice is Pumps 
if(v==1) {
//Assigning features of the variables
type_code.hidden = false;
class_code.hidden = false;
shift_label_open_date.hidden = false;
shift_open_date.hidden = false;
meter_generated.hidden = false;
meters.hidden = false;
opening_meter_no.hidden = false;
Opening_Meter_No1.hidden = false;
Opening_Meter_No2.hidden = false;
Opening_Variance.hidden = false;
lubricants.hidden = true;
opening_label_lubes.hidden = true;
Opening_No_Lubes.hidden = true;
Staff_Allocated_shift.hidden = false;
staff_code.hidden = false;
 
 }
 //When Choice is Lubricants    
else if(v==2) {
  //Assigning features of the variables
type_code.hidden = false;
class_code.hidden = false;
shift_label_open_date.hidden = false;
shift_open_date.hidden = false;
meter_generated.hidden = false;
meters.hidden = true;
opening_meter_no.hidden = true;
Opening_Meter_No1.hidden = true;
Opening_Meter_No2.hidden = true;
Opening_Variance.hidden = true;
lubricants.hidden = false;
opening_label_lubes.hidden = false;
Opening_No_Lubes.hidden = false;
Staff_Allocated_shift.hidden = false;
staff_code.hidden = false;
  }
  //When Choice is neither Pump or Lubricant or Gas
  else{
  //Assigning features of the variables
type_code.hidden = false;
class_code.hidden = false;
shift_label_open_date.hidden = true;
shift_open_date.hidden = true;
meter_generated.hidden = true;
meters.hidden = true;
opening_meter_no.hidden = true;
Opening_Meter_No1.hidden = true;
Opening_Meter_No2.hidden = true;
Opening_Variance.hidden = true;
lubricants.hidden = true;
opening_label_lubes.hidden = true;
Opening_No_Lubes.hidden = true;
Staff_Allocated_shift.hidden = true;
staff_code.hidden = true;
  }}
//******************************************************************************************************************************
//end county loader
function load_years(){   
//    alert("oj");
//Receiving variables
var type_code = document.getElementById("type_code");
var class_code = document.getElementById("class_code");
var shift_label_open_date = document.getElementById("shift_label_open_date");
var shift_open_date = document.getElementById("shift_open_date");
var meter_generated = document.getElementById("meter_generated");
var meters = document.getElementById("meters");
var opening_meter_no = document.getElementById("opening_meter_no");
var Opening_Meter_No1 = document.getElementById("Opening_Meter_No1");
var Opening_Meter_No2 = document.getElementById("Opening_Meter_No2");
var Opening_Variance = document.getElementById("Opening_Variance");
var lubricants = document.getElementById("lubricants");
//*******************tables***************************************
var opening_label_lubes = document.getElementById("opening_label_lubes");
var Opening_No_Lubes = document.getElementById("Opening_No_Lubes");
//****************************end of tables**********************
var Staff_Allocated_shift = document.getElementById("Staff_Allocated_shift");
var staff_code = document.getElementById("staff_code");

//Assigning features of the variables
type_code.hidden = false;
class_code.hidden = false;
shift_label_open_date.hidden = true;
shift_open_date.hidden = true;
meter_generated.hidden = true;
meters.hidden = true;
opening_meter_no.hidden = true;
Opening_Meter_No1.hidden = true;
Opening_Meter_No2.hidden = true;
Opening_Variance.hidden = true;
lubricants.hidden = true;
opening_label_lubes.hidden = true;
Opening_No_Lubes.hidden = true;
Staff_Allocated_shift.hidden = true;
staff_code.hidden = true;


// window.open("districtchooser?county="+dist.value);     
var xmlhttp;    

if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("type_code").innerHTML=xmlhttp.responseText;

document.getElementById("week").innerHTML="<option>loading..</option>";
}
}
xmlhttp.open("POST","Petrofill_Category_loader",true);
xmlhttp.send();
}
//************************************************Loading Staff**********************************************************************************
function load_staff(){    
var xmlhttp;    

if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("staff_code").innerHTML=xmlhttp.responseText;

document.getElementById("week").innerHTML="<option>loading..</option>";
}
}
xmlhttp.open("POST","Petrofill_Staff_loader",true);
xmlhttp.send();
}
//************************************************End of loading Staff*************************************************************************************

function filter_name_item_description(type_code){
disabler();
load_staff();
var petrofill_type=type_code.value; 
var xmlhttp;    
if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{

document.getElementById("class_code").innerHTML= xmlhttp.responseText;;

}
}
xmlhttp.open("POST","Petrofill_class_loader?petrofill_type="+petrofill_type,true);
xmlhttp.send();
}    
        </script> 
        
       
	 <SCRIPT language=Javascript>
		function compare(){
                var inputdate = new Date($('#shift_open_date').val());



                var dateOBj= new Date();

                var currdate=dateOBj.getDate();
                var curmonth=dateOBj.getMonth()+1;
                var curyear=dateOBj.getFullYear();
        
                var today=curyear+"/"+curmonth+"/"+currdate;    
                var todate = new Date(today); 

                //alert(todate+"  "+inputdate);

                if (inputdate < todate && inputdate!=null && inputdate!="")

                {

                    // Do something
                    alert("Date should not be in the past");
                    $('#shift_open_date').focus();
                    return false;

                }
        
            }  
</SCRIPT> 
    </head>