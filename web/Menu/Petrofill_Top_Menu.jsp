<%-- 
    Document   : Petrofill_Top_Menu
    Created on : Jun 21, 2015, 11:21:53 PM
    Author     : Hare Cheka Arnold
--%>

<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
 <nav class="navbar navbar-default navbar-static-top">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
         <button type="button" class="navbar-toggle navbar-toggle-sidebar collapsed">
         MENU
         </button>
         <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
         </button>
         <a class="navbar-brand" href="javascript:void(0)">
           <%=session.getAttribute("PetrofillStation").toString()%>
         </a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">      
         <form class="navbar-form navbar-left" method="GET" role="search">
            <div class="form-group">
               <input type="text" name="q" class="form-control" placeholder="Search">
            </div>
            <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
         </form>
         <ul class="nav navbar-nav navbar-right">
             <li><a> <i class="fa fa-bell fa-2x"></i>  Events <span class="badge">6</span></a></li>
            <li>
                <a  href="javascript:void(0)" class="text-uppercase"> 
                <%if (session.getAttribute("fullname")!=null){ %>
              
                <b>Hi</b>, <%=session.getAttribute("fullname").toString()%>
                    
                 <%}%>
                </a>
            </li>
            <li><a  href="javascript:void(0)"><b>Branch ::</b> <%=session.getAttribute("PetrofillBranch").toString()%> </a></li>
            <li class="dropdown ">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                  Account
                  <span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                     <li class="dropdown-header">SETTINGS</li>
                     <li class=""><a href="#">Other Link</a></li>
                     <li class=""><a href="#">Other Link</a></li>
                     <li class=""><a href="#">Other Link</a></li>
                     <li class="divider"></li>
                     <li><a href="Petrofill_Log_out.jsp">Logout</a></li>
                  </ul>
               </li>
            </ul>
         </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
   </nav>  
