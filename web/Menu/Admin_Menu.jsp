<%-- 
    Document   : Admin_Menu
    Created on : Jun 21, 2015, 11:22:38 PM
    Author     : Hare Cheka Arnold
--%>

<nav class="navbar navbar-default" role="navigation">
         <!-- Main Menu -->
         <div class="side-menu-container">
            <ul class="nav navbar-nav">
               <li class="active"><a href="Petrofill_Dashboard.jsp"><span class="glyphicon glyphicon-dashboard"></span> Dashboard</a></li>
                 <li class="panel panel-default" id="dropdown">
                  <a data-toggle="collapse" href="#dropdown-lvl1">
                     <span class="fa fa-money"></span> Money <span class="caret"></span>
                  </a>

                  <!-- Dropdown level 1 -->
                  <div id="dropdown-lvl1" class="panel-collapse collapse">
                     <div class="panel-body">
                        <ul class="nav navbar-nav">
                         <!-- Dropdown level 2 -->
                           <li class="panel panel-default" id="dropdown">
                              <a data-toggle="collapse" href="#dropdown-lvl2">
                                 <span class="glyphicon glyphicon-off"></span> Money In <span class="caret"></span>
                              </a>
                              <div id="dropdown-lvl2" class="panel-collapse collapse">
                                 <div class="panel-body">
                                    <ul class="nav navbar-nav">
                                        <li><a href="Petrofill_Cust_Orders.jsp" >Customer Orders</a></li> 
                                        <li><a href="Petrofill_Cash_Sales.jsp" >Cash Sales</a></li>
                                        <li><a href="Petrofill_Quotation.jsp" >Quotation</a></li>
                                        <li><a href="Petrofill_Invoices.jsp" >Invoices</a></li> 
                                        <li><a href="Petrofill_Receipts.jsp">Receipts</a></li>
                                    </ul>
                                 </div>
                              </div>
                           </li>
                             <li class="panel panel-default" id="dropdown">
                              <a data-toggle="collapse" href="#dropdown-lvl2">
                                 <span class="glyphicon glyphicon-off"></span> Money Out <span class="caret"></span>
                              </a>
                              <div id="dropdown-lvl2" class="panel-collapse collapse">
                                 <div class="panel-body">
                                    <ul class="nav navbar-nav">
                                       <li><a href="Petrofill_Petty_Cash.jsp"  >Petty Cash</a></li> 
                                        <li><a href="Petrofill_Purchase_Orders.jsp" >Purchase Orders</a></li>
                                        <li><a href="Petrofill_Invoices_Payable.jsp" >Invoices Payable</a></li>
                                        <li><a href="Petrofill_Payments.jsp"  >Payments</a></li>
                                    </ul>
                                 </div>
                              </div>
                           </li>
                        </ul>
                     </div>
                  </div>
               </li>
               
               <li class="panel panel-default" id="dropdown">
                  <a data-toggle="collapse" href="#dropdown-lvl3">
                     <span class="fa fa-bank"></span> Banking <span class="caret"></span>
                  </a>

                  <!-- Dropdown level 1 -->
                  <div id="dropdown-lvl3" class="panel-collapse collapse">
                     <div class="panel-body">
                        <ul class="nav navbar-nav">
                          <li><a href="Petrofill_Transactions.jsp"  >Transactions</a></li> 
                        <li><a href="Petrofill_Reconciliation.jsp" >Reconciliation</a></li>
                        <li><a href="Petrofill_Bank_Accounts.jsp" >Bank Accounts</a></li>
                        </ul>
                     </div>
                  </div>
               </li>
               
                <li class="panel panel-default" id="dropdown">
                  <a data-toggle="collapse" href="#dropdown-lvl5">
                     <span class="fa fa-archive"></span> Assets &  Loans <span class="caret"></span>
                  </a>

                  <!-- Dropdown level 1 -->
                  <div id="dropdown-lvl5" class="panel-collapse collapse">
                     <div class="panel-body">
                        <ul class="nav navbar-nav">
                          <li><a href="Petrofill_Transactions.jsp"  >Transactions</a></li> 
                        <li><a href="Petrofill_Reconciliation.jsp" >Reconciliation</a></li>
                        <li><a href="Petrofill_Bank_Accounts.jsp" >Bank Accounts</a></li>
                        </ul>
                     </div>
                  </div>
               </li>
               
               <li class="panel panel-default" id="dropdown">
                  <a data-toggle="collapse" href="#dropdown-lvl6">
                     <span class="fa fa-server"></span> Inventory <span class="caret"></span>
                  </a>

                  <!-- Dropdown level 1 -->
                  <div id="dropdown-lvl6" class="panel-collapse collapse">
                     <div class="panel-body">
                        <ul class="nav navbar-nav">
                         <!-- Dropdown level 2 -->
                     <li><a href="Petrofill_Inventory.jsp"  >Inventory List</a></li> 
                    <li><a href="Petrofill_Branch_Inventory.jsp" >Branch Inventory</a></li>

                             <li class="panel panel-default" id="dropdown">
                              <a data-toggle="collapse" href="#dropdown-lvl7">
                                 <span class="glyphicon glyphicon-off"></span>Manage Inventory  <span class="caret"></span>
                              </a>
                              <div id="dropdown-lvl7" class="panel-collapse collapse">
                                 <div class="panel-body">
                                    <ul class="nav navbar-nav">
                           <li><a href="Petrofill_Receive_Inventory.jsp"  >Receive Inventory</a></li> 
                          <li><a href="Petrofill_Adjust_Inventory.jsp" >Adjust Inventory</a></li>
                         <li><a href="Petrofill_Transfer_Inventory.jsp" >Transfer Inventory</a></li> 
                                    </ul>
                                 </div>
                              </div>
                           </li>
                        </ul>
                     </div>
                  </div>
               </li>
                  <li class="panel panel-default" id="dropdown">
                  <a data-toggle="collapse" href="#dropdown-lvl8">
                     <span class="fa fa-users"></span> Shifts <span class="caret"></span>
                  </a>

                  <!-- Dropdown level 1 -->
                  <div id="dropdown-lvl8" class="panel-collapse collapse">
                     <div class="panel-body">
                        <ul class="nav navbar-nav">
                          <li><a href="Petrofill_shift_allocation.jsp"  >Shift Allocation</a></li>
                         <li><a href="Petrofill_shift_closing.jsp" >Shift Closing</a></li>
                        </ul>
                     </div>
                  </div>
               </li>
                   <li class="panel panel-default" id="dropdown">
                  <a data-toggle="collapse" href="#dropdown-lvl9">
                     <span class="fa fa-cogs"></span> Settings <span class="caret"></span>
                  </a>

                  <!-- Dropdown level 1 -->
                  <div id="dropdown-lvl9" class="panel-collapse collapse">
                     <div class="panel-body">
                        <ul class="nav navbar-nav">
                          <li><a href="Petrofill_shift_allocation.jsp"  >Users</a></li>
                         
                             <li class="panel panel-default" id="dropdown">
                              <a data-toggle="collapse" href="#dropdown-lv20">
                                 <span class="glyphicon glyphicon-off"></span>Products  <span class="caret"></span>
                              </a>
                              <div id="dropdown-lv20" class="panel-collapse collapse">
                                 <div class="panel-body">
                                    <ul class="nav navbar-nav">
                          <li><a href="Petrofill_Add_Category.jsp" >Add Category</a></li>
                            <li><a href="Petrofill_Add_SubCateg.jsp" >Add Sub Category</a></li>
                            <li><a href="Petrofill_Add_Item.jsp" >Add Item</a></li> 
                                    </ul>
                                 </div>
                              </div>
                           </li>
                           <li><a href="Petrofill_Add_Account.jsp"  >Bank accounts</a></li>
                           <li><a href="Petrofill_Audit_Log.jsp.jsp"  >Audit Log</a></li>
                        </ul>
                     </div>
                  </div>
               </li>
               
                  <li class="panel panel-default" id="dropdown">
                  <a data-toggle="collapse" href="#dropdown-lv21">
                     <span class="fa fa-bar-chart-o"></span> Reports <span class="caret"></span>
                  </a>

                  <!-- Dropdown level 1 -->
                  <div id="dropdown-lv21" class="panel-collapse collapse">
                     <div class="panel-body">
                        <ul class="nav navbar-nav">
                            <li><a href="Petrofill_Income&Expenditure.jsp"  >Income & Expenditure</a></li> 
                        <li><a href="Petrofill_Client_Statements.jsp" >Client Statements</a></li>
                        <li><a href="Petrofill_Supplier_Statements.jsp" >Supplier Statements</a></li>
                        <li><a href="Petrofill_Inventory_Report.jsp" >Inventory Report</a></li>
                        <li><a href="Petrofill_Tax_Report.jsp" >Tax Report</a></li> 
                        <li><a href="Petrofill_Acc_Report.jsp" >Account Aging Report</a></li>
                        <li><a href="Petrofill_Audit_Log.jsp" >Audit Log</a></li> 
                        </ul>
                     </div>
                  </div>
               </li>
              
              
               

               <li><a href="Petrofill_Log_out.jsp"><span class="glyphicon glyphicon-log-out"></span> Log out</a></li>

            </ul>
         </div><!-- /.navbar-collapse -->
      </nav>
