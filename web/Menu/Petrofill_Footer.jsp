<%-- 
    Document   : Petrofill_Footer
    Created on : Jun 21, 2015, 11:07:08 PM
    Author     : Hare Cheka Arnold
--%>

<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Calendar cal = Calendar.getInstance();
    int year = cal.get(Calendar.YEAR);

%>
<footer class="pull-left footer">
    <p class="col-md-12">
    <hr class="divider">
    <center>Copyright &COPY; <%=year%> </center>
</p>
</footer>

<script type="text/javascript" src="js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<!--<link href="css/style.css" media="screen" rel="stylesheet" type="text/css" />-->

<!--<script src="menu/prefix-free.js"></script>-->  
<script type="text/javascript" src="js/jquery-1.9.1.js"></script>
<script src="js/bootstrap.js"></script>
<script type="text/javascript" src="js/noty/jquery.noty.js"></script>

<script type="text/javascript" src="js/noty/layouts/top.js"></script>
<script type="text/javascript" src="js/noty/layouts/center.js"></script>
<!-- You can add more layouts if you want -->
<script type="text/javascript" src="js/noty/themes/default.js"></script>
<script src = "bio_js/jquery-ui-1.10.3.custom.js" ></script>
<script type="text/javascript" src="js/app.js"></script>