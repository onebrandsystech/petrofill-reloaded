-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 11, 2015 at 12:19 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `petrofill`
--
CREATE DATABASE IF NOT EXISTS `petrofill` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `petrofill`;

-- --------------------------------------------------------

--
-- Table structure for table `audit`
--

DROP TABLE IF EXISTS `audit`;
CREATE TABLE IF NOT EXISTS `audit` (
  `action_id` int(50) NOT NULL AUTO_INCREMENT,
  `action` text NOT NULL,
  `time` varchar(100) NOT NULL,
  `actor_id` int(11) NOT NULL,
  `host_comp` text NOT NULL,
  PRIMARY KEY (`action_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=134 ;

--
-- Dumping data for table `audit`
--

INSERT INTO `audit` (`action_id`, `action`, `time`, `actor_id`, `host_comp`) VALUES
(25, 'Logged in ', '2015-1-29-23:56:40', 456233, 'HareChekaArnold 127.0.0.1'),
(26, 'Logged in ', '2015-1-30-19:10:42', 456233, 'HareChekaArnold 192.168.1.62'),
(27, 'Logged in ', '2015-1-31-14:11:34', 456233, 'HareChekaArnold 192.168.1.81'),
(28, 'Logged in ', '2015-1-31-15:25:11', 456233, 'HareChekaArnold 192.168.1.81'),
(29, 'Logged in ', '2015-1-31-20:36:41', 456233, 'HareChekaArnold 127.0.0.1'),
(30, 'Logged out ', '2015-1--31-21-5-35', 456233, 'HareChekaArnold'),
(31, 'Logged in ', '2015-1-31-21:5:44', 456233, 'HareChekaArnold 127.0.0.1'),
(32, 'Logged out ', '2015-1--31-21-5-49', 456233, 'HareChekaArnold'),
(33, 'Logged in ', '2015-1-31-21:11:19', 456233, 'HareChekaArnold 127.0.0.1'),
(34, 'Logged out ', '2015-1--31-21-11-26', 456233, 'HareChekaArnold'),
(35, 'Logged in ', '2015-1-31-21:12:11', 456233, 'HareChekaArnold 127.0.0.1'),
(36, 'Logged out ', '2015-1--31-23-52-36', 456233, 'HareChekaArnold'),
(37, 'Logged in ', '2015-1-31-23:52:53', 456233, 'HareChekaArnold 192.168.43.5'),
(38, 'Logged out ', '2015-2--1-0-6-3', 456233, 'HareChekaArnold'),
(39, 'Logged in ', '2015-2-1-22:16:21', 456233, 'HareChekaArnold 192.168.42.130'),
(40, 'Logged in ', '2015-2-2-21:45:33', 456233, 'HareChekaArnold 127.0.0.1'),
(41, 'Logged out ', '2015-2--2-21-54-34', 456233, 'HareChekaArnold'),
(42, 'Logged in ', '2015-2-2-21:54:43', 456233, 'HareChekaArnold 127.0.0.1'),
(43, 'Logged in ', '2015-2-2-23:7:49', 456233, 'HareChekaArnold 127.0.0.1'),
(44, 'Logged in ', '2015-2-2-23:49:43', 456233, 'HareChekaArnold 127.0.0.1'),
(45, 'Logged out ', '2015-2--3-1-49-21', 456233, 'HareChekaArnold'),
(46, 'Logged in ', '2015-2-3-18:34:54', 456233, 'HareChekaArnold 192.168.1.63'),
(47, 'Logged out ', '2015-2--3-19-50-10', 456233, 'HareChekaArnold'),
(48, 'Logged in ', '2015-2-3-22:41:47', 456233, 'HareChekaArnold 127.0.0.1'),
(49, 'Logged in ', '2015-2-3-23:12:52', 456233, 'HareChekaArnold 127.0.0.1'),
(50, 'Logged out ', '2015-2--3-23-27-41', 456233, 'HareChekaArnold'),
(51, 'Logged in ', '2015-2-3-23:28:3', 456233, 'HareChekaArnold 127.0.0.1'),
(52, 'Logged in ', '2015-2-4-8:29:51', 456233, 'HareChekaArnold 127.0.0.1'),
(53, 'Logged in ', '2015-2-4-21:24:42', 456233, 'HareChekaArnold 192.168.43.147'),
(54, 'Logged out ', '2015-2--4-21-25-15', 456233, 'HareChekaArnold'),
(55, 'Logged in ', '2015-2-4-21:25:47', 456233, 'HareChekaArnold 192.168.43.147'),
(56, 'Logged in ', '2015-2-4-22:46:19', 456233, 'HareChekaArnold 127.0.0.1'),
(57, 'Logged out ', '2015-2--4-23-0-22', 456233, 'HareChekaArnold'),
(58, 'Logged in ', '2015-2-4-23:0:32', 456233, 'HareChekaArnold 127.0.0.1'),
(59, 'Logged out ', '2015-2--5-0-26-14', 456233, 'HareChekaArnold'),
(60, 'Logged in ', '2015-2-5-0:29:20', 456233, 'HareChekaArnold 127.0.0.1'),
(61, 'Logged out ', '2015-2--5-0-50-24', 456233, 'HareChekaArnold'),
(62, 'Logged in ', '2015-2-5-0:50:32', 456233, 'HareChekaArnold 192.168.42.130'),
(63, 'Logged in ', '2015-2-5-0:50:42', 456233, 'HareChekaArnold 192.168.42.130'),
(64, 'Logged in ', '2015-2-5-0:51:22', 456233, 'HareChekaArnold 192.168.42.130'),
(65, 'Logged in ', '2015-2-5-0:51:29', 456233, 'HareChekaArnold 192.168.42.130'),
(66, 'Logged in ', '2015-2-5-0:53:19', 456233, 'HareChekaArnold 192.168.42.130'),
(67, 'Logged out ', '2015-2--5-1-21-34', 456233, 'HareChekaArnold'),
(68, 'Logged in ', '2015-2-5-1:21:54', 456233, 'HareChekaArnold 192.168.42.130'),
(69, 'Logged out ', '2015-2--5-1-24-54', 456233, 'HareChekaArnold'),
(70, 'Logged in ', '2015-2-5-9:14:11', 456233, 'HareChekaArnold 127.0.0.1'),
(71, 'Logged in ', '2015-2-6-14:14:19', 456233, 'HareChekaArnold 127.0.0.1'),
(72, 'Logged out ', '2015-2--6-14-38-45', 456233, 'HareChekaArnold'),
(73, 'Logged in ', '2015-2-8-10:48:6', 456233, 'HareChekaArnold 192.168.42.130'),
(74, 'Logged in ', '2015-2-8-12:1:44', 456233, 'HareChekaArnold 192.168.42.130'),
(75, 'Logged in ', '2015-2-8-13:0:12', 456233, 'HareChekaArnold 192.168.42.130'),
(76, 'Logged in ', '2015-2-8-22:35:30', 456233, 'HareChekaArnold 192.168.42.130'),
(77, 'Logged in ', '2015-2-8-23:20:5', 456233, 'HareChekaArnold 192.168.42.130'),
(78, 'Logged in ', '2015-2-9-0:39:35', 456233, 'HareChekaArnold 192.168.42.130'),
(79, 'Logged out ', '2015-2--9-0-44-31', 456233, 'HareChekaArnold'),
(80, 'Logged in ', '2014-12-31-19:58:32', 456233, 'HareChekaArnold 192.168.1.2'),
(81, 'Logged out ', '2014-12--31-19-59-56', 456233, 'HareChekaArnold'),
(82, 'Logged in ', '2014-12-31-22:52:42', 456233, 'HareChekaArnold 127.0.0.1'),
(83, 'Logged in ', '2015-2-12-11:51:56', 34566, 'HareChekaArnold 192.168.0.126'),
(84, 'Logged out ', '2015-2--12-11-52-8', 34566, 'HareChekaArnold'),
(85, 'Logged in ', '2015-2-12-22:4:24', 34566, 'HareChekaArnold 127.0.0.1'),
(86, 'Logged in ', '2015-2-12-23:1:19', 34566, 'HareChekaArnold 127.0.0.1'),
(87, 'Logged out ', '2015-2--13-0-54-26', 34566, 'HareChekaArnold'),
(88, 'Logged in ', '2015-2-13-0:54:35', 34566, 'HareChekaArnold 127.0.0.1'),
(89, 'Logged out ', '2015-2--13-1-6-49', 34566, 'HareChekaArnold'),
(90, 'Logged in ', '2015-2-14-21:54:20', 34566, 'HareChekaArnold 127.0.0.1'),
(91, 'Logged in ', '2015-2-14-23:1:53', 34566, 'HareChekaArnold 127.0.0.1'),
(92, 'Logged in ', '2015-2-15-1:21:44', 34566, 'HareChekaArnold 127.0.0.1'),
(93, 'Logged out ', '2015-2--15-1-29-28', 34566, 'HareChekaArnold'),
(94, 'Logged in ', '2015-2-15-1:52:24', 34566, 'HareChekaArnold 127.0.0.1'),
(95, 'Logged out ', '2015-2--15-1-56-53', 34566, 'HareChekaArnold'),
(96, 'Logged in ', '2015-2-15-15:41:25', 34566, 'HareChekaArnold 192.168.42.130'),
(97, 'Logged in ', '2015-2-15-18:28:18', 34566, 'HareChekaArnold 127.0.0.1'),
(98, 'Logged in ', '2015-2-15-19:52:4', 34566, 'HareChekaArnold 127.0.0.1'),
(99, 'Logged in ', '2015-2-16-1:46:40', 34566, 'HareChekaArnold 127.0.0.1'),
(100, 'Logged in ', '2015-2-16-19:6:19', 34566, 'HareChekaArnold 192.168.0.115'),
(101, 'Logged out ', '2015-2--16-19-8-33', 34566, 'HareChekaArnold'),
(102, 'Logged in ', '2015-2-16-23:34:32', 34566, 'HareChekaArnold 127.0.0.1'),
(103, 'Logged out ', '2015-2--16-23-51-55', 34566, 'HareChekaArnold'),
(104, 'Logged in ', '2015-2-17-0:1:11', 34566, 'HareChekaArnold 127.0.0.1'),
(105, 'Logged in ', '2015-2-17-0:10:56', 34566, 'HareChekaArnold 127.0.0.1'),
(106, 'Logged out ', '2015-2--17-0-11-15', 34566, 'HareChekaArnold'),
(107, 'Logged in ', '2015-2-18-18:20:50', 34566, 'HareChekaArnold 192.168.1.62'),
(108, 'Logged in ', '2015-2-18-19:13:43', 34566, 'HareChekaArnold 192.168.1.62'),
(109, 'Logged in ', '2015-2-18-23:10:34', 34566, 'HareChekaArnold 127.0.0.1'),
(110, 'Logged out ', '2015-2--18-23-13-35', 34566, 'HareChekaArnold'),
(111, 'Logged in ', '2015-2-20-7:58:50', 34566, 'HareChekaArnold 127.0.0.1'),
(112, 'Logged out ', '2015-2--20-7-59-0', 34566, 'HareChekaArnold'),
(113, 'Logged in ', '2015-3-7-13:45:51', 34566, 'HareChekaArnold 192.168.1.2'),
(114, 'Logged out ', '2015-3--7-13-46-19', 34566, 'HareChekaArnold'),
(115, 'Logged in ', '2015-3-7-15:14:23', 34566, 'HareChekaArnold 192.168.1.2'),
(116, 'Logged out ', '2015-3--7-15-18-16', 34566, 'HareChekaArnold'),
(117, 'Logged in ', '2015-3-8-15:22:48', 34566, 'HareChekaArnold 127.0.0.1'),
(118, 'Logged out ', '2015-3--8-15-30-53', 34566, 'HareChekaArnold'),
(119, 'Logged in ', '2015-3-18-23:24:28', 34566, 'HareChekaArnold 127.0.0.1'),
(120, 'Logged out ', '2015-3--18-23-27-28', 34566, 'HareChekaArnold'),
(121, 'Logged in ', '2015-3-29-13:5:41', 34566, 'HareChekaArnold 192.168.42.2'),
(122, 'Logged out ', '2015-3--29-13-9-25', 34566, 'HareChekaArnold'),
(123, 'Logged in ', '2015-3-29-21:43:9', 34566, 'HareChekaArnold 127.0.0.1'),
(124, 'Logged out ', '2015-3--29-22-20-29', 34566, 'HareChekaArnold'),
(125, 'Logged in ', '2015-4-1-8:46:11', 34566, 'HareChekaArnold 127.0.0.1'),
(126, 'Logged out ', '2015-4--1-9-8-3', 34566, 'HareChekaArnold'),
(127, 'Logged in ', '2015-4-8-23:24:33', 34566, 'HareChekaArnold 192.168.42.130'),
(128, 'Logged in ', '2015-9-19-0:24:34', 34566, 'user-PC 127.0.0.1'),
(129, 'Logged in ', '2015-10-11-1:2:38', 34566, 'user-PC 127.0.0.1'),
(130, 'Logged out ', '2015-10--11-1-4-4', 34566, 'user-PC'),
(131, 'Logged in ', '2015-10-11-1:4:15', 34566, 'user-PC 127.0.0.1'),
(132, 'Logged out ', '2015-10--11-1-4-19', 34566, 'user-PC'),
(133, 'Logged in ', '2015-10-11-1:4:29', 34566, 'user-PC 127.0.0.1');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `Category_ID` int(50) NOT NULL AUTO_INCREMENT,
  `Category_Name` varchar(200) NOT NULL,
  PRIMARY KEY (`Category_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`Category_ID`, `Category_Name`) VALUES
(1, 'Pump'),
(2, 'Lubes'),
(3, 'Gas');

-- --------------------------------------------------------

--
-- Table structure for table `category_class`
--

DROP TABLE IF EXISTS `category_class`;
CREATE TABLE IF NOT EXISTS `category_class` (
  `Category_Class_ID` int(50) NOT NULL AUTO_INCREMENT,
  `Category_Class_Name` varchar(200) NOT NULL,
  `Category_ID` int(50) NOT NULL,
  PRIMARY KEY (`Category_Class_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `category_class`
--

INSERT INTO `category_class` (`Category_Class_ID`, `Category_Class_Name`, `Category_ID`) VALUES
(1, 'Diesel', 1),
(2, 'Super', 1),
(3, 'Kerosene', 1),
(4, 'Focal Point(A)', 2),
(5, 'Focal Point(B)', 2),
(6, 'Focal Point Gas(A)', 3),
(7, 'Focal Point Gas(B)', 3),
(8, 'Focal Point Gas(C)', 3),
(9, 'Low Sulphur Diesel', 1),
(10, 'Unleaded Diesel', 1);

-- --------------------------------------------------------

--
-- Table structure for table `petrol_branch_details`
--

DROP TABLE IF EXISTS `petrol_branch_details`;
CREATE TABLE IF NOT EXISTS `petrol_branch_details` (
  `Branch_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Branch_Name` varchar(200) NOT NULL,
  `Station_Name` varchar(200) NOT NULL,
  PRIMARY KEY (`Branch_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `petrol_inventory`
--

DROP TABLE IF EXISTS `petrol_inventory`;
CREATE TABLE IF NOT EXISTS `petrol_inventory` (
  `Petrol_Transaction_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `Orderline_ID` int(11) NOT NULL,
  `Transaction_Type` int(1) NOT NULL COMMENT '1=Receiving: 2=Adjusting: 3=Transfering',
  `Transaction_Items` int(11) NOT NULL COMMENT 'Shows Item''s ID',
  `Transaction_User` int(11) NOT NULL COMMENT 'Shows User''s ID',
  `Transaction_Date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Shows the Transaction Date',
  `Transaction_Comment` varchar(250) NOT NULL COMMENT 'Either Received or Adjusted or Transfered',
  `Transaction_Inventory` int(11) NOT NULL COMMENT 'Shows amount changed',
  PRIMARY KEY (`Petrol_Transaction_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `petrol_items`
--

DROP TABLE IF EXISTS `petrol_items`;
CREATE TABLE IF NOT EXISTS `petrol_items` (
  `Item_Name` varchar(250) NOT NULL,
  `Item_SubCategory_ID` int(11) NOT NULL,
  `Supplier_ID` int(11) NOT NULL,
  `Item_Code` varchar(250) NOT NULL,
  `Description` varchar(250) NOT NULL,
  `Cost_Price` double(15,2) NOT NULL,
  `Unit-Price` double(15,2) NOT NULL,
  `Quantity` double(15,2) NOT NULL,
  `ReOrder-Level` double(15,2) NOT NULL,
  `Location` varchar(250) NOT NULL,
  `Item_ID` int(11) NOT NULL,
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Item_Status` int(11) NOT NULL,
  `Log_Item` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `petrol_items_taxes`
--

DROP TABLE IF EXISTS `petrol_items_taxes`;
CREATE TABLE IF NOT EXISTS `petrol_items_taxes` (
  `Item_ID` int(10) NOT NULL AUTO_INCREMENT,
  `Name_Tax` varchar(250) NOT NULL COMMENT 'If vatable or not',
  `Tax_Percentage` double(15,2) NOT NULL,
  `Delete_Status` tinyint(5) NOT NULL,
  PRIMARY KEY (`Item_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `petrol_item_categroy`
--

DROP TABLE IF EXISTS `petrol_item_categroy`;
CREATE TABLE IF NOT EXISTS `petrol_item_categroy` (
  `Item_Category_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Item_Category_Name` varchar(250) NOT NULL,
  `Item_Category_Description` varchar(250) NOT NULL,
  `Item_Categroy_Status` int(1) NOT NULL COMMENT '1=Active: 0=Suspended',
  `Item_Category_Log` int(11) NOT NULL DEFAULT '0',
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`Item_Category_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `petrol_item_categroy`
--

INSERT INTO `petrol_item_categroy` (`Item_Category_ID`, `Item_Category_Name`, `Item_Category_Description`, `Item_Categroy_Status`, `Item_Category_Log`, `Timestamp`) VALUES
(1, 'Petroleum', 'made of oil', 1, 0, '2015-02-15 16:25:55'),
(2, 'Food', 'All About food', 1, 0, '2015-02-15 16:25:55');

-- --------------------------------------------------------

--
-- Table structure for table `petrol_item_location_inventory`
--

DROP TABLE IF EXISTS `petrol_item_location_inventory`;
CREATE TABLE IF NOT EXISTS `petrol_item_location_inventory` (
  `Item_InventID` int(11) NOT NULL AUTO_INCREMENT,
  `Item_ID` int(11) NOT NULL,
  `Location_ID` int(11) NOT NULL,
  `Quantity` double NOT NULL,
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`Item_InventID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `petrol_item_subcategory`
--

DROP TABLE IF EXISTS `petrol_item_subcategory`;
CREATE TABLE IF NOT EXISTS `petrol_item_subcategory` (
  `Item_SubCategory_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Item_Category_ID` int(11) NOT NULL COMMENT 'Foreign Key',
  `Item_SubCategory_Name` varchar(250) NOT NULL,
  `Item_SubCategory_Description` varchar(250) NOT NULL,
  `Item_SubCategory_Status` int(1) NOT NULL COMMENT '1=Active: 0=Suspended: 3=Deleted',
  `Item_SubCategory_Log` int(11) NOT NULL DEFAULT '0',
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`Item_SubCategory_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `petrol_pricebook`
--

DROP TABLE IF EXISTS `petrol_pricebook`;
CREATE TABLE IF NOT EXISTS `petrol_pricebook` (
  `Pricebook_ID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `Branch_ID` int(11) NOT NULL,
  `Item_ID` int(11) NOT NULL,
  `Cost_Price` double NOT NULL,
  `Retail_Price` double NOT NULL,
  `Discount` double NOT NULL,
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Available` text NOT NULL COMMENT 'Extra column just incase',
  PRIMARY KEY (`Pricebook_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `petrol_station_people`
--

DROP TABLE IF EXISTS `petrol_station_people`;
CREATE TABLE IF NOT EXISTS `petrol_station_people` (
  `Fname` varchar(200) NOT NULL,
  `Lname` varchar(200) NOT NULL,
  `Gender` varchar(200) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `Email` varchar(200) NOT NULL,
  `ID_Number` int(50) NOT NULL,
  `DOB` varchar(200) NOT NULL,
  `Address` varchar(200) NOT NULL,
  `Pin_No` varchar(200) NOT NULL,
  `People_Branch_ID` int(10) NOT NULL,
  `User_ID` int(10) NOT NULL AUTO_INCREMENT,
  `User_Level` int(10) NOT NULL,
  `People_Type` int(1) NOT NULL DEFAULT '0' COMMENT '1=Customer: 0=Employee: 2=Administratorn: 3= Supplier',
  `People_Status` int(10) NOT NULL,
  `Log` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`User_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `petrol_station_people`
--

INSERT INTO `petrol_station_people` (`Fname`, `Lname`, `Gender`, `phone`, `Email`, `ID_Number`, `DOB`, `Address`, `Pin_No`, `People_Branch_ID`, `User_ID`, `User_Level`, `People_Type`, `People_Status`, `Log`) VALUES
('Admin', 'Admin', 'Male', '0770391180', 'hrrnld@yahoo.com', 27285010, '28/11/1988', 'Nairobi', 'A00546GK', 3, 1, 2, 3, 1, 'eve'),
('User', 'user', 'Female', '0717212724', 'hrrnld@yahoo.com', 27285011, '29/11/1989', 'Nakuru', 'ASDF4666', 2, 2, 2, 3, 1, 'Adam');

-- --------------------------------------------------------

--
-- Table structure for table `petrol_suppliers`
--

DROP TABLE IF EXISTS `petrol_suppliers`;
CREATE TABLE IF NOT EXISTS `petrol_suppliers` (
  `Supplier_ID` int(11) NOT NULL,
  `Company_Name` varchar(250) NOT NULL,
  `Account_Number` varchar(250) NOT NULL,
  `Delete_Status` int(1) NOT NULL COMMENT 'Shows Delete Status'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `petrol_suppliers`
--

INSERT INTO `petrol_suppliers` (`Supplier_ID`, `Company_Name`, `Account_Number`, `Delete_Status`) VALUES
(2, 'Oil Refinery', '123456789', 0),
(1, 'Kengen', '987654321', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` varchar(50) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `mname` varchar(50) NOT NULL DEFAULT '  ',
  `lname` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(200) NOT NULL,
  `level` varchar(50) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `fname`, `mname`, `lname`, `phone`, `username`, `password`, `level`, `timestamp`) VALUES
('1247218', 'user', '  ', 'user', '0722432465', 'user', 'ee11cbb19052e40b07aac0ca060c23ee', '2', '2014-09-05 21:36:11'),
('1614093', 'Maxwell', '  Rotich', 'Melany', '0716896332', 'Maxwell', 'c026548b7fa4b1f772f2f59374b93bba', '2', '2014-08-29 00:50:49'),
('166386', 'Brian', '  ', 'Nyangena', '0727773342', 'nyangs', '9607dab47c87edbc4fcc0a3479687e17', '1', '2014-09-22 12:38:00'),
('1764344', 'makoha', '  ', 'makoha', '0726896532', 'makoha', '8156c82a642630474f95a3099f545706', '1', '2014-09-22 14:36:52'),
('34566', 'user', 'user', 'user', '0711111111', 'Bismak', 'f9c823d11634b73f0d699b151f7a7bf6', '1', '2014-09-22 06:32:37'),
('456233', 'Arnold', 'Cheka', 'Hare', '0717212724', 'Cheka', 'ee11cbb19052e40b07aac0ca060c23ee', '1', '2014-08-27 22:18:40'),
('488752', 'Frank', 'Kavoi', 'Kavoi', '0729522222', 'Winnie', 'da4c5332661cad24dc34553651312cda', '1', '2014-08-31 13:24:50'),
('611238', 'blessed', '  ', 'blessed', '0722569832', 'blessed', '940e126f00a5c2f4bcc0179581798b52', '1', '2014-09-24 08:17:24'),
('95344', 'Sophie', 'Hare', 'Mgeni', '0704968729', 'Sophie', 'd432ee38d61da527e8e5f542d64cd43d', '2', '2014-08-26 18:42:44');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
