-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 11, 2015 at 12:20 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `main_petrofill`
--
CREATE DATABASE IF NOT EXISTS `main_petrofill` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `main_petrofill`;

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
CREATE TABLE IF NOT EXISTS `country` (
  `Country_ID` int(50) NOT NULL AUTO_INCREMENT,
  `Country_Name` varchar(200) NOT NULL,
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Status` int(10) NOT NULL,
  PRIMARY KEY (`Country_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `petrol_branch`
--

DROP TABLE IF EXISTS `petrol_branch`;
CREATE TABLE IF NOT EXISTS `petrol_branch` (
  `BranchID` int(50) NOT NULL AUTO_INCREMENT,
  `Branch_Name` varchar(200) NOT NULL,
  `Petrol_Station_ID` int(50) NOT NULL,
  `Status` int(10) NOT NULL,
  `Log` int(50) NOT NULL,
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`BranchID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `petrol_stations`
--

DROP TABLE IF EXISTS `petrol_stations`;
CREATE TABLE IF NOT EXISTS `petrol_stations` (
  `Petrol_Station_ID` int(50) NOT NULL AUTO_INCREMENT,
  `Petrol_Name` varchar(200) NOT NULL,
  `Petrol_Email` varchar(200) NOT NULL,
  `Pf_Name` varchar(200) NOT NULL,
  `Pl_Name` varchar(200) NOT NULL,
  `Pphone` varchar(200) NOT NULL,
  `Pmobile` varchar(200) NOT NULL,
  `Website` varchar(200) NOT NULL,
  `Dname` varchar(200) NOT NULL,
  `Vat_no` varchar(200) NOT NULL,
  `Pin_no` varchar(200) NOT NULL,
  `Country` int(10) NOT NULL,
  `Petrol_Station_Status` int(10) NOT NULL,
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Password` varchar(400) NOT NULL,
  `Address` varchar(200) NOT NULL,
  PRIMARY KEY (`Petrol_Station_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `petrol_stations`
--

INSERT INTO `petrol_stations` (`Petrol_Station_ID`, `Petrol_Name`, `Petrol_Email`, `Pf_Name`, `Pl_Name`, `Pphone`, `Pmobile`, `Website`, `Dname`, `Vat_no`, `Pin_no`, `Country`, `Petrol_Station_Status`, `Timestamp`, `Password`, `Address`) VALUES
(1, 'BISMAK', 'bismak@gmail.com', 'Bismak', 'LTD', '0770391180', '0770391180', 'www.bismak.com', 'jdbc:mysql://localhost:3306/petrofill', '00000BIS', '11111BIS', 0, 1, '2015-02-11 20:36:05', 'f9c823d11634b73f0d699b151f7a7bf6', 'Bismak nakuru');

-- --------------------------------------------------------

--
-- Table structure for table `petrol_station_login`
--

DROP TABLE IF EXISTS `petrol_station_login`;
CREATE TABLE IF NOT EXISTS `petrol_station_login` (
  `PSID` int(50) NOT NULL,
  `PSLID` int(50) NOT NULL AUTO_INCREMENT,
  `PUsername` varchar(200) NOT NULL,
  `Ppassword` varchar(200) NOT NULL,
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `UserID` int(50) NOT NULL,
  PRIMARY KEY (`PSLID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `petrol_station_login`
--

INSERT INTO `petrol_station_login` (`PSID`, `PSLID`, `PUsername`, `Ppassword`, `Timestamp`, `UserID`) VALUES
(1, 1, 'Bismak', 'f9c823d11634b73f0d699b151f7a7bf6', '2015-02-12 08:49:46', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
