/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package PetroSource;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Hare Cheka Arnold
 */
public class PetroGlobal {
    
    //Constructor
    public PetroGlobal(){
		
	}
    
    /*Functions that are used globally in the system*/
    public int DoInserts(String PassedSQL) throws SQLException{
        
        //Initialising the integer returned
        int TheUID = 0;
        
        //Calling the db object
        DbPertroconn conn = new DbPertroconn();
        
        //Executing the insertion
        conn.st3.executeUpdate(PassedSQL.trim());
        
        //Getting the columns
        conn.rs3=conn.st3.getGeneratedKeys();
        
        //Checking the condition
        if (conn.rs3.next()){
           TheUID=conn.rs3.getInt(1);
       }
        //Closing the connections
         conn.rs3.close();
       conn.st3.close();
      
       //The return statement
        return TheUID;
        
    }
    
    /*************Function for doing updates*****************/
    public boolean DoUpdates(String PassedSQL) throws SQLException{
    
        //Calling the db object
        DbPertroconn conn = new DbPertroconn();
        
        //Executing the updating
        conn.st1.executeUpdate(PassedSQL.trim());
        
        //Closing the connections
        conn.st1.close();
        
        //The return statement
        return true;
    }
    
    /***************Function for doing fetch*****************/
    public int DoFetch(String PassedSQL) throws SQLException{
        
        //Initialising the count
        int mycount=0;
    
        //Calling the db Object
         DbPertroconn conn = new DbPertroconn();
         
         //Executing the Fetch
         conn.rs5 = conn.st5.executeQuery(PassedSQL.trim());
         
         //Executing the while statement
          while(conn.rs5.next())
       {
     	   mycount= conn.rs5.getInt(1);
       }
          //Closing connection
          conn.rs5.close();
          conn.st5.close();
        
          //Return statement
        return mycount;
                  
    }
    
    /*****************Function to Return an integer******************/
    public int DoReturnInt(String PassedSQL, String ColumName) throws SQLException{
    
    //Initialising returned int
    int RetunInt=0;
    
    //Calling the db Object
    DbPertroconn conn = new DbPertroconn();
    
    //Executing the Fetch
    conn.rs5 = conn.st5.executeQuery(PassedSQL.trim());
    
    //Executing the while statement
    while(conn.rs5.next())
        {
     	   RetunInt= conn.rs5.getInt(ColumName);
       }
    //Closing connection
    conn.rs5.close();
    conn.st5.close();
    //The Return Statement
        return RetunInt;
    }
    
    /************************Function to Return String**********************/
    public String DoReturnString(String PassedSQL, String ColumName) throws SQLException{
    
    //Initialising returned int
    String ReturnString="";
    
    //Calling the db Object
    DbPertroconn conn = new DbPertroconn();
    
    //Executing the Fetch
    conn.rs5 = conn.st5.executeQuery(PassedSQL.trim());
    
    //Executing the while statement
    while(conn.rs5.next())
        {
     	   ReturnString= conn.rs5.getString(ColumName);
       }
    //Closing connection
    conn.rs5.close();
    conn.st5.close();
    //The Return Statement
        return ReturnString;
    }
    
    
    /************************Function to delete Record*********************/
    public void DeleteRecord(String PassedSQL) throws SQLException{
    
        //Calling the db Object
        DbPertroconn conn = new DbPertroconn();
        
        //Executing the updating
        conn.st1.executeUpdate(PassedSQL.trim());
        
        //Closing connection
        conn.st5.close();        
        
    }
    
}
