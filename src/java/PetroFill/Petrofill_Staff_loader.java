/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package PetroFill;


import PetroSource.DbPertroconn;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Hare Cheka Arnold
 */
public class Petrofill_Staff_loader extends HttpServlet {

         //Initialises the variables
String petrofill_type_result,petrofill_type,petrofill_first_name,petrofill_last_name;
//Initialises the httprequest of the class
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        //Executes the try function
        try {
            //Creatig db connection
           DbPertroconn conn = new DbPertroconn();
           //Receiving the parameters
           petrofill_type=request.getParameter("petrofill_type");
           petrofill_first_name = "";
           petrofill_last_name = "";
           //Getting the name of the Staff
           String Staff_name = "SELECT * FROM petrol_station_people WHERE User_Level != 1";
                     
          
           
           System.out.println(""+petrofill_first_name+" "+petrofill_last_name);
           //Declares the variable to null
          petrofill_type_result="<option value=\"\">Choose Staff Name</option>";
          
           conn.rs=conn.st.executeQuery(Staff_name);
           while(conn.rs.next()){
                petrofill_first_name = conn.rs.getString("Fname");
                petrofill_last_name = conn.rs.getString("Lname");
                String Full_name = ""+petrofill_first_name+" "+petrofill_last_name+"";
            petrofill_type_result+= "<option value=\""+conn.rs.getString("User_ID")+"\">"+Full_name+"</option>"; 
           
           System.out.println(Full_name);
           System.out.println(petrofill_type_result);
           }
if(conn.rs!=null){
            conn.rs.close();
            }
if(conn.st!=null){
            conn.st.close();
            }
            out.println("<html>");
            out.println("<head>");
            out.println("<title></title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>,"+petrofill_type_result+ ",</h1>");
            out.println("</body>");
            out.println("</html>");
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(Petrofill_Staff_loader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(Petrofill_Staff_loader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
