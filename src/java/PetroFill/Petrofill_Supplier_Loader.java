/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package PetroFill;


import PetroSource.DbPertroconn;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Hare Cheka Arnold
 */
public class Petrofill_Supplier_Loader extends HttpServlet {

    String county;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
           DbPertroconn conn = new DbPertroconn();
           county="<option value=\"\">Choose Item Supplier</option>";
           
           String county_selector="SELECT * FROM petrol_suppliers JOIN petrol_station_people ON petrol_suppliers.Supplier_ID = petrol_station_people.User_ID WHERE petrol_station_people.People_Status = 1";
           conn.rs=conn.st.executeQuery(county_selector);
           System.out.println(county_selector);
           while(conn.rs.next()){
            county+="<option value=\""+conn.rs.getString("Supplier_ID")+"\">"+conn.rs.getString("Company_Name")+"</option>";     
            System.out.println(county);   
           }
if(conn.rs!=null){
            conn.rs.close();
            }
if(conn.st!=null){
            conn.st.close();
            }
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet county_loader</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>" +county+ "</h1>");
            out.println("</body>");
            out.println("</html>");
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(Petrofill_Supplier_Loader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(Petrofill_Supplier_Loader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
