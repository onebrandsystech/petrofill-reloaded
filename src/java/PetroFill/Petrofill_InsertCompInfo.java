/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package PetroFill;

import PetroSource.DbPertroconn;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Hare Cheka Arnold
 */
public class Petrofill_InsertCompInfo extends HttpServlet {

     /*Initialising variables*/
    HttpSession session;
    String Company_Name, Company_Address,Company_Email,Company_Pin,Company_Vat,Company_Phone,Company_ShiftDay1,Company_ShiftDay2,
           Company_PerWeek13,Company_PerWeek12,Company_PerWeek14,Company_PerWeek7,Company_PerWeek6;
    int Company_Action_Taken;
    int User_Action = 0;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, JSONException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
     
        //*************************************************Requesting session**********************************************************
            session = request.getSession();
//***************************************************Receiving parameters from Form*******************************************
            Company_Name = request.getParameter("Company_Name");
            Company_Address = request.getParameter("Company_Address");
            Company_Email = request.getParameter("Company_Email");
            Company_Pin = request.getParameter("Company_Pin");
            Company_Vat = request.getParameter("Company_Vat");
            Company_Phone = request.getParameter("Company_Phone");
            Company_ShiftDay1 = request.getParameter("Company_ShiftDay1");
            Company_ShiftDay2 = request.getParameter("Company_ShiftDay2");
            Company_PerWeek13 = request.getParameter("Company_PerWeek13");
            Company_PerWeek12 = request.getParameter("Company_PerWeek12");
            Company_PerWeek14 = request.getParameter("Company_PerWeek14");
            Company_PerWeek7 = request.getParameter("Company_PerWeek7");
            Company_PerWeek6 = request.getParameter("Company_PerWeek6");
            Company_Action_Taken = Integer.parseInt(request.getParameter("User_Action"));
           
            /**********************************Making a JSON OBJECT****************************************/
             JSONObject Company_Info = new JSONObject();
             
             //Populating the JSON Object
             Company_Info.accumulate("Company_Address", Company_Address);
             Company_Info.accumulate("Company_Email", Company_Email);
             Company_Info.accumulate("Company_Pin", Company_Pin);
             Company_Info.accumulate("Company_Vat", Company_Vat);
             Company_Info.accumulate("Company_Phone", Company_Phone);
             Company_Info.accumulate("Company_ShiftDay1", Company_ShiftDay1);
             Company_Info.accumulate("Company_ShiftDay2", Company_ShiftDay2);
             Company_Info.accumulate("Company_PerWeek13", Company_PerWeek13);
             Company_Info.accumulate("Company_PerWeek12", Company_PerWeek12);
             Company_Info.accumulate("Company_PerWeek14", Company_PerWeek14);
             Company_Info.accumulate("Company_PerWeek7", Company_PerWeek7);
             Company_Info.accumulate("Company_PerWeek6", Company_PerWeek6);
             
            /***********Creating a new object for the data source*************************/
            DbPertroconn conn = new DbPertroconn();
            
            /************************SQL Statements****************************/
            String Comp_Info_Edit = "UPDATE company_info SET Comp_Name = '"+Company_Name+"',Comp_InfoJson = "
                    + "'"+Company_Info.toString()+"' WHERE Comp_ID = "
                    + "'"+ session.getAttribute("userid").toString()+"'";
            
            String Comp_Info_Insert = "INSERT INTO company_info (Comp_Name,Comp_InfoJson) "
                + "VALUES ('" + Company_Name + "','" + Company_Info.toString() + "')";
            
            /************************Determiner Which Button Has Been Pressed*******************/
            if (Company_Action_Taken == 1){
                //Updating the User_Action
                User_Action = Company_Action_Taken;
                
                //Executing the SQL Saving Statement
                conn.st1.executeUpdate(Comp_Info_Insert);
                
                //Putting into session
                session.setAttribute("Company_Editing", "<font color=\"green\"> Company Information "+Company_Name+" Saved Succesfully</font>");
                
            }else{
                //Executing the SQL Updating Statemeent
                conn.st1.executeUpdate(Comp_Info_Edit);
                
                //Putting into session
                session.setAttribute("Company_Editing", "<font color=\"green\">Company Information "+Company_Name+" Edited Succesfully</font>");
                
                            
            }
            //Redirecting to the the same Editing Page
            response.sendRedirect("Company_Info_Details.jsp");
        
     
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
                    try {
                        processRequest(request, response);
                    } catch (SQLException ex) {
                        Logger.getLogger(Petrofill_InsertCompInfo.class.getName()).log(Level.SEVERE, null, ex);
                    }
        } catch (JSONException ex) {
            Logger.getLogger(Petrofill_InsertCompInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
                    try {
                        processRequest(request, response);
                    } catch (SQLException ex) {
                        Logger.getLogger(Petrofill_InsertCompInfo.class.getName()).log(Level.SEVERE, null, ex);
                    }
        } catch (JSONException ex) {
            Logger.getLogger(Petrofill_InsertCompInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
