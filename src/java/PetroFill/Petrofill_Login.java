/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package PetroFill;

import PetroSource.DbMainPetroConn;
import PetroSource.DbPertroconn;
import java.io.IOException;
import java.math.BigInteger;
import java.net.InetAddress;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Hare Cheka Arnold
 */
public class Petrofill_Login extends HttpServlet {

     String uname,pass,error_login,nextPage,current_time;
    String computername;
    MessageDigest m;
    String full_name;
    HttpSession session;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, NoSuchAlgorithmException, SQLException {
        
    //Creating a new object of the main database connection
        DbMainPetroConn connn = new DbMainPetroConn();

        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH) + 1;
        int date = cal.get(Calendar.DATE);
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int min = cal.get(Calendar.MINUTE);
        int sec = cal.get(Calendar.SECOND);
        String yr, mnth, dater, hr, mn, sc, action = "";
        yr = Integer.toString(year);
        mnth = Integer.toString(month);
        dater = Integer.toString(date);
        hr = Integer.toString(hour);
        mn = Integer.toString(min);
        sc = Integer.toString(sec);
        session = request.getSession();

//____________________COMPUTER NAME____________________________________
        computername = InetAddress.getLocalHost().getHostName();
        System.out.println("Computer name " + computername);
        session.setAttribute("computer_name", computername);
        
        //Setting session holding the branch name
        session.setAttribute("PetrofillBranch", PetrofillGetBranch());
        session.setAttribute("PetrofillStation", PetrofillGetStation());





//current_time=sc+"-"+mn+"-"+hr+"-"+dater+"-"+mnth+"-"+yr;

//current_time=yr+"-"+mnth+"-"+dater+"-"+hr+":"+mn+":"+sc;

        current_time = yr + "-" + mnth + "-" + dater + "-" + hr + ":" + mn + ":" + sc;
        //get username and password

        uname = request.getParameter("uname");
        System.out.println("user name " + uname);

        pass = request.getParameter("pass");
        System.out.println("Paasword " + pass);






        //encrypt password

        m = MessageDigest.getInstance("MD5");
        m.update(pass.getBytes(), 0, pass.length());
        String pw = new BigInteger(1, m.digest()).toString(16);
        
        //**************************************Getting Data from the Main Database*********************************************
        String ps_get_login_data = "select * from petrol_station_login";
        
        //Executing the query
        connn.rs = connn.st.executeQuery(ps_get_login_data);
        
        //If exist search a row
        while (connn.rs.next()) {
            
            String Petrol_Station_ID = connn.rs.getString("PSID");
            
            //Check if thea exist
             if (connn.rs.getString("PUsername").equals(uname) && connn.rs.getString("Ppassword").equals(pw)) {
                 
                 //Getting details of the petrol Station
                 String ps_get_PS_data = "SELECT * FROM petrol_stations WHERE Petrol_Station_ID = '"+Petrol_Station_ID+"' ";
                 System.out.println(Petrol_Station_ID);
                 //Executing the Query
                 connn.rs1 = connn.st1.executeQuery(ps_get_PS_data);
                 
                 //If exist such a record
                 while (connn.rs1.next()) {
                 session.setAttribute("petrolname", connn.rs1.getString("Petrol_Name"));
                 session.setAttribute("petrolemail", connn.rs1.getString("Petrol_Email"));
                 session.setAttribute("petrolfnem", connn.rs1.getString("Pf_Name"));
                 session.setAttribute("petrollnem", connn.rs1.getString("Pl_Name"));
                 session.setAttribute("petrol4n", connn.rs1.getString("Pphone"));
                 session.setAttribute("petrolmbl", connn.rs1.getString("Pmobile"));
                 session.setAttribute("petrolwesyt", connn.rs1.getString("Website"));
                 session.setAttribute("petroldbnem", connn.rs1.getString("Dname"));
                 session.setAttribute("petrolvat", connn.rs1.getString("Vat_no"));
                 session.setAttribute("petrolpin", connn.rs1.getString("Pin_no"));
                 session.setAttribute("petrolcntry", connn.rs1.getString("Country"));
                 session.setAttribute("petroladdrss", connn.rs1.getString("Address"));
                 session.setAttribute("petrolpetrolstatus", connn.rs1.getString("Petrol_Station_Status"));
                 
                 System.out.println(session.getAttribute("petroldbnem"));
                 }
        //**************************************End of getting the data*********************************************************
//Requesting session
                  //Requesting session
        session = request.getSession();

        //connection to database class instance
        DbPertroconn conn = new DbPertroconn();

        //query for checking user existance in the database
        String select1 = "select * from users";
        






        conn.rs = conn.st.executeQuery(select1);
        System.out.println("Query " + select1);

        System.out.println("username:"+uname+"  Password :"+pw );
         


        while (conn.rs.next()) {
            System.out.println("This is "+conn.rs.getString("password") );
            if (conn.rs.getString("username").equals(uname) && conn.rs.getString("password").equals(pw)) {

                error_login = null;
                if (conn.rs.getString("level").equals("1")) {
                    String ip = InetAddress.getLocalHost().getHostAddress();
                      System.out.println("level:"+conn.rs.getString("level"));
                    String inserter = "insert into audit set host_comp='" + computername + " " + ip + "' , action='Logged in ',time='" + current_time + "',actor_id='" + conn.rs.getString("user_id") + "'";

                    //String inserter="insert into audit  (action,time,actor_id,host_comp) values ('"++"','"+"')";

                    conn.st3.executeUpdate(inserter);
                    //the next page to be opened based on user level
                    nextPage = "Petrofill_Dashboard.jsp";


//String fulname=""+conn.rs.getString("firstname") + " "+conn.rs.getString("lastname");
//audit="Insert into audit (Action,User) values('Logged in','"+fulname+"')";



                    //save current user details into a session

                   
                    session.setAttribute("fname", conn.rs.getString("fname"));
                    session.setAttribute("lname", conn.rs.getString("lname"));
                    session.setAttribute("level", conn.rs.getString("level"));
                    session.setAttribute("userid", conn.rs.getString("user_id"));
                    session.setAttribute("username", conn.rs.getString("username"));
                    String fulname = (session.getAttribute("fname") +" "+ session.getAttribute("lname"));
                    session.setAttribute("fullname", fulname);
                   

                    System.out.println( session.getAttribute("fname")+" __"+session.getAttribute("lname"));
                    
                    //get teacher details from the teacher registration table 


                    /** code for auditing  */
                    // conn.st.executeUpdate(audit);
                    break;
                }//end of admin level
                //****************************Clerk module**********************************************        
                else if (conn.rs.getString("level").equals("2")) {
                    // System.out.println("level 2");      
                    nextPage = "Petrofill_Dashboard.jsp";



                    session.setAttribute("userid", conn.rs.getString(1));
                    session.setAttribute("username", conn.rs.getString("username"));
                    session.setAttribute("level", conn.rs.getString("level"));
                      session.setAttribute("fname", conn.rs.getString("fname"));
                    session.setAttribute("lname", conn.rs.getString("lname"));
                    String fulname = (session.getAttribute("fname") +" "+ session.getAttribute("lname"));
                    session.setAttribute("fullname", fulname);
                    //save other session details to dbase

                    String clerk = "select * from users";

                    conn.rs1 = conn.st1.executeQuery(clerk);

                    while (conn.rs1.next()) {

                        if (conn.rs1.getString("user_id").equals(session.getAttribute("user_id"))) {

                            session.setAttribute("f_name", conn.rs1.getString("fname"));
                            session.setAttribute("s_name", conn.rs1.getString("lname"));
                            String ip = InetAddress.getLocalHost().getHostAddress();
                            String inserter = "insert into audit set host_comp='" + computername + " " + ip + "' , action='Logged in',time='" + current_time + "',actor_id='" + conn.rs.getString("user_id") + "'";
                            
                            conn.st3.executeUpdate(inserter);

                            break;
                        }

                    }
                    error_login = "<b><font color=\"red\">ooops! wrong username and / or password combination</font></b>";

                    break;

                } //         ^^^^^^^^^^^^^^^^ IF  USER EXIST  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^              
                else if (conn.rs.getString("level").equals("3")) {
                    nextPage = "Petrofill_Dashboard.jsp";



                    session.setAttribute("userid", conn.rs.getString(1));
                    session.setAttribute("username", conn.rs.getString("username"));
                    session.setAttribute("level", conn.rs.getString(""
                            + "level"));
                      session.setAttribute("fname", conn.rs.getString("fname"));
                    session.setAttribute("lname", conn.rs.getString("lname"));
                    //save other session details to dbase

                    String guest = "select * from users";

                    conn.rs = conn.st.executeQuery(guest);

                    while (conn.rs.next()) {

                        if (conn.rs.getString("userid").equals(session.getAttribute("user_id"))) {
                            session.setAttribute("who", "guest");
                            session.setAttribute("f_name", conn.rs.getString("fname"));
                            session.setAttribute("lname", conn.rs.getString("lname"));
                            session.setAttribute("username", conn.rs.getString("username"));
                            String ip = InetAddress.getLocalHost().getHostAddress();
                            String inserter = "insert into audit set host_comp='" + computername + " " + ip + "' , action='Logged in(guest)',time='" + current_time + "',actor_id='" + conn.rs.getString("user_id") + "'";
                            conn.st3.executeUpdate(inserter);

                            break;
                        }

                    }
                    error_login = "<b><font color=\"red\">ooops! wrong username and / or password combination</font></b>";

                    break;
                } //****************************wrong username password                        
                else {

                    nextPage = "index.jsp";

                    System.out.println("third level");

                    error_login = "<b><font color=\"red\">ooops! wrong username and / or password combination</font></b>";

                }








            }//end of first if
            else {

                //System.out.println("worked up to here 6;");

                nextPage = "index.jsp";

                error_login = "<b><font color=\"red\">wrong username and or password</font></b>";

                System.out.println(">>" + nextPage);

            }



        }
         }
             else{
                 nextPage = "index.jsp";

                error_login = "<b><font color=\"red\">wrong username and or password</font></b>";

                System.out.println(">>" + nextPage);
             
            }
        }


        session.setAttribute("error_login", error_login);
        response.sendRedirect(nextPage);
       
    }
public String PetrofillGetBranch() throws SQLException{
    DbPertroconn conn = new DbPertroconn();
    String Branch_Name = "";
    String get_branch = "SELECT * FROM petrol_branch_details WHERE Branch_ID = '"+1+"'";
    conn.rs3 = conn.st3.executeQuery(get_branch);
    while(conn.rs3.next()){
    Branch_Name = conn.rs3.getString("Branch_Name");
    }
        return Branch_Name;

}
public String PetrofillGetStation() throws SQLException{
    DbPertroconn conn = new DbPertroconn();
    String Branch_Name = "";
    String get_branch = "SELECT * FROM petrol_branch_details WHERE Branch_ID = '"+1+"'";
    conn.rs3 = conn.st3.executeQuery(get_branch);
    while(conn.rs3.next()){
    Branch_Name = conn.rs3.getString("Station_Name");
    }
        return Branch_Name;

}
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Petrofill_Login.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Petrofill_Login.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Petrofill_Login.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Petrofill_Login.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

   
}